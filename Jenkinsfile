#!/usr/bin/env groovy
 
/**
 * The Jenkins file for generating rpm and uploading to Pulp.
 * 1. Maven Build and Package
 * 2. Upload it to mobi-snapshot pulp repo
 */
node('sonar') {
    def currentBuildStage
    def project_name = env["PROJECT_NAME"] ?: 'mobi-udm-content-provider-console'
    def artifactFilter = env["ARTIFACT_FILTER"] ?: '**/target/rpm/**/RPMS/noarch/*.rpm' // filter to get a file to archive.
    def currentdirectory = pwd()
    def gitBranch = env.GIT_BRANCH_NAME ?: 'master'
    def rpmfilepath
    def rpmname
    def repo_id = env["REPO_ID"] ?: 'mobi-snapshots'
      
    properties([buildDiscarder(logRotator(artifactDaysToKeepStr: '', artifactNumToKeepStr: '', daysToKeepStr: '10', numToKeepStr: '10')), [$class: 'RebuildSettings', autoRebuild: false, rebuildDisabled: false], pipelineTriggers([])])
    File file = new File("${WORKSPACE}/rpmexist")
    // email function for triggering email
        try {
        currentBuildStage = 'Checkout the repository'
        stage("${currentBuildStage}") {
            // checkout repository
        git branch: env.BRANCH_NAME, credentialsId: '6ed9601c-884d-492f-b38f-50f63114a1be', url: 'git@bitbucket.org:mobitv/mobi-udm-content-provider-console.git'
        }
         
        if (env.BRANCH_NAME == 'master'){
        currentBuildStage = 'Packaging'
        stage("${currentBuildStage}") {
                echo 'Building the project and creating rpm'
                sh 'mvn -s /home/build/.m2/settings.xml -Dmaven.repo.local=/home/build/.m2/repository -U -Prpm -Pgitversion clean deploy'
                echo 'Getting rpmname in file'         
                dir ("target/rpm/${project_name}/RPMS/noarch/"){
                sh "ls | grep ${project_name} > rpmfilename"
                rpmname = readFile('rpmfilename')
                }     
                rpmfilepath = "${currentdirectory}/target/rpm/${project_name}/RPMS/noarch/${rpmname}"
            }
        currentBuildStage = 'Uploading rpm to mobi-snapshot repo'
        stage("${currentBuildStage}") {
                echo 'login to pulp repo'
                sh "pulp-admin login -u admin -p admin"
                //checking the rpm
                echo "checking the rpm in ${repo_id} repo "
                sh "pulp-admin rpm repo content rpm --repo-id=${repo_id} --match filename=${rpmname} > ${WORKSPACE}/rpmexist"          
                if (file.length() == 0){
                    echo "uploading rpm "
                    sh "pulp-admin rpm repo uploads rpm --repo-id ${repo_id} -f ${rpmfilepath}"
                    echo 'publishing rpm'
                    sh "pulp-admin rpm repo publish run --repo-id ${repo_id}"
                    echo "Successfully logout from pulp repo"
                    sh 'pulp-admin logout'
                }
                else {
                    // RPM exist IN mobi-snapshot repo
                   echo "${rpmname} exist in ${repo_id} repo.No need to upload into pulp repo"
                   sh "pulp-admin logout"
                   error("Build failed because ${rpmname} exist in ${repo_id}")
                    }
               }
        currentBuildStage = 'Archive and publishing Junit results'
        stage("${currentBuildStage}") {
                echo 'archiving'
                archive "target/**/*"
                step([$class: 'JUnitResultArchiver', testResults: '**/target/surefire-reports/TEST-*.xml'])
             }
        currentBuildStage = 'Publishing Code coverage on jenkins'
        stage("${currentBuildStage}") {
                step( [ $class: 'JacocoPublisher' ] )
                 }
        currentBuildStage = 'Sending Results to Sonarqube'
        stage("${currentBuildStage}") {
                sh 'mvn -s /home/build/.m2/settings.xml -Dmaven.repo.local=/home/build/.m2/repository sonar:sonar'
                }
        }   
        else{
            currentBuildStage = 'Building the Plan with Snapshot Version rpm'
            stage("${currentBuildStage}") {
                echo 'Compiling the project'
                sh 'mvn -s /home/build/.m2/settings.xml -Dmaven.repo.local=/home/build/.m2/repository -U -DskipTests=true -Prpm -Pgitversion clean deploy'
                echo 'Getting rpmname in file'
                echo "${BRANCH_NAME}"
                dir ("target/rpm/${project_name}-${BRANCH_NAME}/RPMS/noarch/"){
                                                                sh "ls | grep ${project_name}-${BRANCH_NAME} > rpmfilename"
                                                                rpmname = readFile('rpmfilename')
                                                        }     
                rpmfilepath = "${currentdirectory}/target/rpm/${project_name}-${BRANCH_NAME}/RPMS/noarch/${rpmname}"
            }
            currentBuildStage = 'Uploading Snapshot rpm to mobi-snapshot repo'
            stage("${currentBuildStage}") {
                        echo 'login to pulp repo'
                        sh "pulp-admin login -u admin -p admin"
                        //checking the rpm
                        echo "checking the rpm in ${repo_id} repo "
                        sh "pulp-admin rpm repo content rpm --repo-id=${repo_id} --match filename=${rpmname} > ${WORKSPACE}/rpmexist"          
                        if (file.length() == 0){
                        echo "uploading rpm "
                        sh "pulp-admin rpm repo uploads rpm --repo-id ${repo_id} -f ${rpmfilepath}"
                        echo 'publishing rpm'
                        sh "pulp-admin rpm repo publish run --repo-id ${repo_id}"
                        echo "Successfully logout from pulp repo"
                        sh 'pulp-admin logout'
                        }
                        else {
                            // RPM exist IN mobi-snapshot repo
                            echo "${rpmname} exist in ${repo_id} repo. No need to upload into pulp repo"
                            sh "pulp-admin logout"
                        }
                    }
            }
        }          
        catch (any) {
            //Cleaning up workspace
            deleteDir()
            // send email
            email(currentBuildStage)                //define email function at the end to send email at failed stage
            throw any
            }
    
        finally {
                deleteDir()                 //always cleanup workspace
            }
             
         
     
    }
         
def email (currentBuildStage) {
                def ccEmailId = 'sbommareddy@mobitv.com'
 
        if (env.BRANCH_NAME == 'master') {
            ccEmailId = 'sbommareddy@mobitv.com'    //If branch name starts with master and release  Choose owner who wants to make in cc regarding build status
             
        }
 
        def recipients = [[$class: 'DevelopersRecipientProvider'],
                          [$class: 'CulpritsRecipientProvider'],
                          [$class: 'FirstFailingBuildSuspectsRecipientProvider'],
                          [$class: 'FailingTestSuspectsRecipientProvider'],
                          [$class: 'UpstreamComitterRecipientProvider']]
        emailext(attachLog: true,
                sendToIndividuals: true,
                subject: "FAILED :: ${currentBuildStage} stage :: " + '$PROJECT_NAME :: Build # $BUILD_NUMBER !!!',
                body: 'Please check the attached build log OR check console output: $BUILD_URL  ### ${BUILD_LOG, maxLines=200, escapeHtml=false}',
                replyTo: '$DEFAULT_REPLYTO',
                recipientProviders: recipients,
                to: "cc:${ccEmailId}")
                 
    }