package com.mobitv.contentprovider;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestContext;
import org.springframework.test.context.web.*;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultMatcher;
import org.springframework.test.web.servlet.setup.DefaultMockMvcBuilder;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import com.mobitv.contentprovider.service.ContentProviderServiceImpl;

import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

@RunWith(MockitoJUnitRunner.class)
@ContextConfiguration(classes = {TestContext.class})
@WebAppConfiguration
public class AbstractContentTest {
	
	private MockMvc mockMvc;

	@Autowired
	private WebApplicationContext webApplicationContext;

	@Autowired
	private ContentProviderServiceImpl contentProviderServiceImplMock;

	@Before
	public void setup() {
		Mockito.reset(contentProviderServiceImplMock);
		this.mockMvc = MockMvcBuilders.webAppContextSetup(this.webApplicationContext).build();
	}
	
//TODO: Dino finish unit tests
	@Ignore
	@Test
	public void getNetworks() throws Exception {
		this.mockMvc.perform(get("/aset/1"))
		.andExpect((ResultMatcher) ((DefaultMockMvcBuilder) status().isOk())
				.alwaysExpect(content().contentType("application/json;charset=UTF-8")));
	}

}
