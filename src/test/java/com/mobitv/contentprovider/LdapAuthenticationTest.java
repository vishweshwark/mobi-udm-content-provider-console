/**
 * 
 */
package com.mobitv.contentprovider;

import java.util.Hashtable;

import javax.naming.Context;
import javax.naming.ldap.InitialLdapContext;

import org.junit.Test;

/**
 * @author sbommareddy
 *
 */
public class LdapAuthenticationTest {
	
	@Test
	public void ldapAuthenticationIsSuccessful() throws Exception {
	    Hashtable<String,String> env = new Hashtable<String,String>();
	    env.put(Context.SECURITY_AUTHENTICATION, "simple");
//	    env.put(Context.SECURITY_PROTOCOL, "ssl");
	    env.put(Context.SECURITY_PRINCIPAL, "uid=cp_user,ou=People,dc=foo,dc=bar");
	    env.put(Context.PROVIDER_URL, "ldap://inldp01p1.ci-cp.dev.smf1.mobitv:389/dc=foo,dc=bar");
	    env.put(Context.SECURITY_CREDENTIALS, "cp_user");
	    env.put(Context.INITIAL_CONTEXT_FACTORY, "com.sun.jndi.ldap.LdapCtxFactory");

	    InitialLdapContext ctx = new InitialLdapContext(env, null);

	}

}
