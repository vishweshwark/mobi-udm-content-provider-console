<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page import="org.springframework.security.ldap.userdetails.LdapUserDetailsImpl" %>
<%@ page import="org.springframework.security.core.context.SecurityContextHolder" %>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link rel="stylesheet" href="<c:url value="/resources/app/lib/bootstrap-3.1.1-dist/css/bootstrap.css" />">
<link rel="stylesheet" href="<c:url value="/resources/app/lib/font-awesome/css/font-awesome.min.css" />">
<link href="<c:url value="/resources/app/lib/angular-motion.min.css"/>" rel="stylesheet" >
<link href="<c:url value="/resources/app/lib/angular-xeditable/css/xeditable.css"/>" rel="stylesheet" >

<link href="<c:url value="/resources/app/css/app.css"/>" rel="stylesheet" >

<title>MobiTV--Content Provider Console</title>
</head>

<body  ng-controller="ContentProviderController">
      <div class="navbar navbar-inverse navbar-fixed-top">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
        </div>
      <div class="container">
        <div class="navbar-collapse collapse" >   
      <img src="/mobi-content-provider-console/resources/app/images/mobi_white_connected.png"/> 

          <font color=white><b>Content Management Console</b></font>
          	<c:url value="/j_spring_security_logout" var="logoutUrl" />
            	      <form class="navbar-form navbar-right" role="form" action="${logoutUrl}" method="post" id="logoutForm">
           <span style="color:white">Welcome ${authenticatedUser}</span>
   
	 <button type="submit" class="btn btn-sm btn-danger">Sign out</button>
	</form>   
        </div><!--/.navbar-collapse -->         
  </div>	 
      </div> 

<br/>
<br/>
<br/>
<br/> 
    <div class="container" ng-view>
</div>

<!--  use RequireJS to manage javascript dependencies 
	  Put at the bottom for faster html loading 
	  -->
<script data-main="<c:url value="/resources/app/js/main" />" src="<c:url value="/resources/app/lib/require.js" />"></script>
<script  src="<c:url value="/resources/app/js/app.js" />"></script>
</body>
</html>