<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@page session="true"%>
<html>
<head>
<title>Login Page</title>
    <!-- Bootstrap core CSS -->
<link rel="stylesheet" href="<c:url value="resources/app/lib/bootstrap-3.1.1-dist/css/bootstrap.css" />">
    <!-- Custom styles for this template -->
    <link href="http://getbootstrap.com/examples/cover/cover.css" rel="stylesheet">
<style>
body {
  padding-top: 40px;
  padding-bottom: 40px;
 // background-color: #eee;
}

.form-signin {
  max-width: 330px;
  padding: 15px;
  margin: 0 auto;
}
.form-signin .form-signin-heading,
.form-signin .checkbox {
  margin-bottom: 10px;
}
.form-signin .checkbox {
  font-weight: normal;
}
.form-signin .form-control {
  position: relative;
  height: auto;
  -webkit-box-sizing: border-box;
     -moz-box-sizing: border-box;
          box-sizing: border-box;
  padding: 10px;
  font-size: 16px;
}
.form-signin .form-control:focus {
  z-index: 2;
}
.form-signin input[type="email"] {
  margin-bottom: -1px;
  border-bottom-right-radius: 0;
  border-bottom-left-radius: 0;
}
.form-signin input[type="password"] {
  margin-bottom: 10px;
  border-top-left-radius: 0;
  border-top-right-radius: 0;
}
</style>

</head>
<body>
    

    <div class="site-wrapper">

      <img src="/mobi-content-provider-console/resources/app/images/mobi_white_connected.png"/> 
        <h1>Content Management Console</h1>
        <p>By using this console you can view and manage VOD assets and metadata.</p>
        <p><a class="btn btn-warning btn-lg" role="button" href="https://mobinet.mobitv.corp/display/engineering/Content+Management+Console">Learn more &raquo;</a></p>
   
        
      <form  name='loginForm' class="form-signin"  action="<c:url value='/j_spring_security_check' />" method='POST'>
      	<span style="color:red">${loginError}</span>
        <input type="text"  name='username' class="form-control" placeholder="LDAP login" required autofocus>
        <input type="password"  name='password'  class="form-control" placeholder="Password" required>
 <!--        <label class="checkbox">
          <input type="checkbox" value="0"  id="remember_me" name="_remember_me" > Remember me
        </label>
         -->
        <button class="btn btn-lg btn-primary btn-block" type="submit">Sign in</button>
      </form>
</div>
</div>
    </div> <!-- /container -->
    <center>
          <div class="mastfoot">
            <div class="inner">
                        <a id="footer-logo" href="/" class="icon-mobitv-logo-light image-link">Mobitv</a>
                        <small>
                            &copy; 2014 MobiTV. All Rights Reserved.
                        </small><a href="http://www.mobitv.com/copyright">Copyright</a><span>|</span>
                            <a href="http://www.mobitv.com/privacy-policy">Privacy Policy</a><span>|</span>
                            <a href="http://www.mobitv.com/terms-of-use">Terms of Use</a><span>|</span>
	                        <a href='http://www.mobitv.com/contact'>Contact</a>
            </div>
          </div>
          </center>
</body>
</html>