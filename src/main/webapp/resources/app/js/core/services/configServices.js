/**
 * User: kliolios
 * Date: 3/4/2015
 */
/*global module:false*/
define([ 'services/services'],
  function (services) {
    'use strict';
    
    services.constant('Config', {
        ControlledVocabularyURL: {
        	REST_PACKAGE_URL: 'rest/admin/v5',
        	ENDPOINT_PATH: 'controlled_vocabulary'
        }
    });

    services.factory('CVUrlCreator', ['Config', function(Config) {
        return function(cv_type) {
          return [Config.ControlledVocabularyURL.REST_PACKAGE_URL, Config.ControlledVocabularyURL.ENDPOINT_PATH, cv_type].join('/');
        };
      }]);
    

});