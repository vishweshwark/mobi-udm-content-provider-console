/**
 * User: kliolios
 * Date: 11/25/14
 */
/*global module:false*/
define(['services/services'],
		function(services) {
'use strict';

services.factory("LoggedUsername", 
		['$http', function($http) {
return {
	get: function() {
			return $http.get('admin/v5/content/ldap/username', {params: {}});
    }

};
}]);

services.factory("LoggedUserDetails", 
		['$http', function($http) {
			return {
				get: function() {
						return $http.get('admin/v5/content/ldap/user', {params: {}});
			    }

			};
			}]);


});