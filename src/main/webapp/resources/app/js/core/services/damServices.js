/**
 * User: kliolios
 * Date: 11/18/14
 */
/*global module:false*/
define(['services/services'],
		function(services) {
'use strict';

services.factory("Networks", ['$http', function($http) {
return {
	get: function() {
			return $http.get('rest/admin/v5/content/networks.json', {params: {}});
    }
};
}]);

services.factory("Network", ['$http', function($http) {
return {
	remove: function(network_cd) {
		return $http.delete('rest/admin/v5/content/network.json', {headers: {'Content-Type': 'application/x-www-form-urlencoded'}, params: {network_code: network_cd}});
},
update: function(network) {
	return $http.put('rest/admin/v5/content/network', network);
}
};
}]);

services.factory("Thumbnail", ['$http', function($http) {
return {
	remove: function(thumbnail_id) {
		return $http.delete('rest/admin/v5/content/thumbnail.json', {params: {thumbnail_id: thumbnail_id}});
}
};
}]);

services.factory("Assets", ['$http', function($http) {
	return {
		get: function(params) {
				return $http.get('rest/admin/v5/content/assets.json?include_artifacts=false', {params: params});
	    }

	};
	}]);	

services.factory("Asset", ['$http', function($http) {
return {
	put: function(asset) {
		return $http.put('rest/admin/v5/content/asset', asset);
	}
};
}]);

services.factory("ContentProviders", ['$http', function ($http) {
    return {
        get: function() {
        	return $http.get('admin/v5/content/ldap/contentprovidergroups/authorized', {});
        }
    };
}]);

});


