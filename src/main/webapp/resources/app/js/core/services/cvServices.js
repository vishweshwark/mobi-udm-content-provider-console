/**
 * User: kliolios
 * Date: 11/18/14
 */
/*global module:false*/
define(['services/services', 'underscore', 'services/configServices'],
		function(services, _) {
'use strict';

services.factory('thumbnailTypesService', ['$http', 'CVUrlCreator',
                                       function ($http, CVUrlCreator) {
                                     	   var cvUrl = CVUrlCreator('thumbnail_types');
                                        return {
                                     	   controlledThumbnailTypes : function () {
                                     		   return $http.get(cvUrl, {params: {}});
                                        }
                                     };
                                     }]);

services.factory('categoriesService', ['$http', 'CVUrlCreator',
  function ($http, CVUrlCreator) {
	   var cvUrl = CVUrlCreator('categories');
   return {
	   controlledCategories : function () {
		   return $http.get(cvUrl, {params: {}});
   }
};
}]);

services.factory('extendedMetadataService', ['$http', 'CVUrlCreator',
     function ($http, CVUrlCreator) {
	   var cvUrl = CVUrlCreator('editable_extended_metadata');
	 return { metadataList : function () {
		   return $http.get(cvUrl, {params: {},  transformResponse: function(data, headers){
	           // response comes as a term_list object as string (Angular deserializes json responses by default)
	           // we need to serialize to a json object and strip out the root element
	           var serialized = angular.fromJson(data);
	           return serialized.term_list;
	           }});
      }
	 };
}]);

services.factory('assetStateService', ['$http', 'CVUrlCreator',
     function ($http, CVUrlCreator) {
	   var cvUrl = CVUrlCreator('asset_states');
       return {controlledAssetStates : function () {
	      return $http.get(cvUrl, {params: {},  transformResponse: function(data, headers){
	           // response comes as a term_list object as string (Angular deserializes json responses by default)
	           // we need to serialize to a json object and strip out the root element
	           var serialized = angular.fromJson(data);
	           return serialized.vocabulary;
	           }});
        }
   };
}]);


services.factory('genresService', ['$http', 'CVUrlCreator',
                                   function ($http, CVUrlCreator) {
	 var cvUrl = CVUrlCreator('genres');
     	 return {controlledGenreList : function () {
     		 return $http.get(cvUrl, {params: {},  transformResponse: function(data, headers){
  	           // response comes as a term_list object as string (Angular deserializes json responses by default)
  	           // we need to serialize to a json object and strip out the root element
  	           var serialized = angular.fromJson(data);
  	           // Underscore.js - Map Array of key/value pairs to an Object 
  	           var map = _.object(_.map(serialized.ontology_nodes, function(x){return [x.category, x.value]}));
  	           return map;
  	           }});
}
	 };
}]);

	});

