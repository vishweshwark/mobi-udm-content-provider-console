define(['filters/filters'],		
function(filters) {
'use strict';

// This function receives the asset from DAM/cms bindings response and changes some of
	// its members objects so they can integrate easily with angular widgets.  
	// It converts comma separated string to arrays (keywords, genres etc.)

    filters.filter('angularizeAsset', function() {
  return function(asset) {	
    		// deserialize
	  // console.log(asset);
            var deserializedAssetString = angular.toJson(asset);
//              console.log(deserializedAssetString);
            var findDate = "\"start_of_availability\":null,\|\"start_of_availability\":.+?,";
            var dateregex = new RegExp(findDate, "g");
            var match;
            while (match = dateregex.exec(deserializedAssetString)) {
                if (match[0] == '"start_of_availability":null,') {
                } else {
                    var UTCseconds = match[0];
                    UTCseconds = UTCseconds.replace('"start_of_availability":', '');
                    UTCseconds = UTCseconds.replace(/,$/, '');
                    var startOfAvailability = new Date(UTCseconds * 1);
                    var arrayfyMatch = JSON.stringify(startOfAvailability);
                    deserializedAssetString = deserializedAssetString.replace(match, '"start_of_availability":' + arrayfyMatch + ',');
              //  	console.log(deserializedAssetString);
                }
            }
            
            var findDate = "\"expires\":null,\|\"expires\":.+?,";
            var dateregex = new RegExp(findDate, "g");
            while (match = dateregex.exec(deserializedAssetString)) {
            //	console.log(match);
                if (match[0] == '"expires":null,') {
                } else {
                    var UTCseconds = match[0];
                    UTCseconds = UTCseconds.replace('"expires":', '');
                    UTCseconds = UTCseconds.replace(/,$/, '');
                    var startOfAvalability = new Date(UTCseconds * 1);
                	//console.log(startOfAvalability);
                    var arrayfyMatch = JSON.stringify(startOfAvalability);
                    deserializedAssetString = deserializedAssetString.replace(match, '"expires":' + arrayfyMatch + ',');
                //	console.log(deserializedAssetString);
                }
            }
            
            var exFind = "\"extended_metadata_list\":null,";
            var exRegex = new RegExp(exFind, "g");
            while (match = exRegex.exec(deserializedAssetString)) {
              //  	console.log(match);
                    deserializedAssetString = deserializedAssetString.replace(match, '"extended_metadata_list":[],');
            }

            var gfind = "\"genre_list\":null,\|\"genre_list\":.+?category";
            var gregex = new RegExp(gfind, "g");
            var sfind = "action serials";
            var ser = new RegExp(sfind);
            while (match = gregex.exec(deserializedAssetString)) {
            	//console.log(match);
                if (match[0] == '"genre_list":null,') {
                    deserializedAssetString = deserializedAssetString.replace(match, '"genre_list":[],');
                } else {
                    var arrayfy = match[0];
                    arrayfy = arrayfy.replace('"genre_list":"', '');
                    arrayfy = arrayfy.replace('\","category', '');
                    arrayfy = arrayfy.replace(/\"/g, '');
                    arrayfy = arrayfy.replace(/\\/g, '');
                    var genres = arrayfy.split(',');
                    for (var i = 0; i < genres.length; i++) {
                        genres[i] = genres[i].replace(/^ /, '');
                        genres[i] = genres[i].replace(/ $/, '');
                    }

                    var arrayfyMatch = JSON.stringify(genres);
                    //	console.log(arrayfyMatch);
                    deserializedAssetString = deserializedAssetString.replace(match, '"genre_list":' + arrayfyMatch + ',"category');
                }
            }


            //    console.log(deserializedAssetString);
            // reserialize
            var angularized = '';
        	try {
        		angularized = angular.fromJson(deserializedAssetString);
        	}
        catch(err) {
        	console.log(err);
            $scope.alerts.push({ type: 'failure', msg: 'Could not serialize to JSON.  Please refresh page and contact MobiTV.' } );
        }
       // console.log(angularized);
        return angularized;
    	}
    })});