define(['filters/filters'],		
function(filters) {
'use strict';    	
    	// This function does exactly the opposite from angularize.js.
    	//  After editing the asset has been completed, the asset object
    	// needs to be reconverted back to its initial form so it can be PUT 
    	// to the DAM APIs.
    	
filters.filter('deangularizeAsset', function() {
  return function(asset) {	
	  
    		// deserialize
            var deserializedAssetString = angular.toJson(asset);
            var match;
            var exFind = "\extended_metadata_list\":[],";
            var exRegex = new RegExp(exFind, "g");
            while (match = exRegex.exec(deserializedAssetString)) {
               // 	console.log(match);
                    deserializedAssetString = deserializedAssetString.replace(match, '"extended_metadata_list":null,');
            }

            var gfind = "\"genre_list\":[]\|\"genre_list\":.+?\]";
            var gregex = new RegExp(gfind, "g");
            while (match = gregex.exec(deserializedAssetString)) {
                //	console.log(match);
                if (match[0] == '"genre_list":[]') {
                    deserializedAssetString = deserializedAssetString.replace(match, '"genre_list":null');
                } else {
                    var arrayfy = match[0];
                    arrayfy = arrayfy.replace('"genre_list":', '');
                    arrayfy = arrayfy.replace(/\]$/, '');
                    arrayfy = arrayfy.replace(/^\[/, '');
                    arrayfy = arrayfy.replace(/\",\" ?/g, ',');
                    deserializedAssetString = deserializedAssetString.replace(match, '"genre_list":' + arrayfy);
                }
            }


            // console.log(deserializedAssetString);
            // reserialize
            var deangularized = '';
            	try {
            		deangularized = angular.fromJson(deserializedAssetString);
            	}
            catch(err) {
            	console.log(err);             
            	//$scope.alerts.push({ type: 'failure', msg: 'Could not serialize to JSON.  Please refresh page and contact MobiTV.' } );
            }
            //console.log(deangularized);
            return deserializedAssetString;
    	}
    });
});