define(['controllers/controllers', 'services/damServices', 'filters/formatFileSize', 'filters/angularize', 'filters/deAngularize', 'services/cvServices'],
		function(controllers) {
	'use strict';
	controllers.controller('AssetModalCtrl', ['$scope', '$rootScope', '$modalInstance', '$timeout', '$filter', '$upload', '$q', '$http', '$log', '$resource', 'assetToEdit', 'Asset', 'Assets', 'Thumbnail', 'thumbnailTypesService', 'genresService', 'extendedMetadataService', function ($scope, $rootScope, $modalInstance, $timeout, $filter, $upload, $q, $http, $log, $resource, assetToEdit, Asset, Assets, Thumbnail, thumbnailTypesService, genresService, extendedMetadataService) {
		$scope.modalAssets = $rootScope.seriesAssets;
		$scope.alerts = [];
		$scope.idsOfRemovedThumbnails = [];
		$scope.primary_thumbnail_id = null;

		$scope.closeAlert = function(index) {
			$scope.alerts.splice(index, 1);
		};

		var angularizeAssetFilter = $filter('angularizeAsset');
		var deangularizeAssetFilter = $filter('deangularizeAsset');
		$scope.assetToEdit = angularizeAssetFilter(assetToEdit);

		angular.forEach($scope.assetToEdit.thumbnails, function (s) {
			if (s.thumbnail_name == 'primary') {
				$scope.primary_thumbnail_id = s.thumbnail_id;
			}
		});

		$scope.edited = false;

		var assetCategories = [];
		$scope.thumbnailTypes = [{"name":"","display_name":"-- Choose Thumbnail Type --"}];
		$scope.thumbnail_name = $scope.thumbnailTypes[0];
		$scope.genres = [];
		$scope.genresOntology = {};

		// Get Thumbnail Types
		thumbnailTypesService.controlledThumbnailTypes().success(function(response) {
			$scope.thumbnailTypes = $scope.thumbnailTypes.concat(response.vocabulary);
		});

		// Get Genre Ontology
		genresService.controlledGenreList().success(function(response) {
			$scope.genresOntology = response;

			if ($scope.assetToEdit.category != null) {
				assetCategories = $scope.assetToEdit.category.split(",");
			}

			for(var i=0; i<assetCategories.length; i++) {

//				call the genres service to get genres for each category
				var genresSubList = $scope.genresOntology[assetCategories[i].trim()];
				if(genresSubList != null && genresSubList.length > 0) {
					$scope.genres = $scope.genres.concat(genresSubList);
				}
			}


			$scope.showGenres = function () {
				var selected = [];
				angular.forEach($scope.genres, function (s) {
					if ($scope.assetToEdit.genre_list.indexOf(s.toLowerCase()) >= 0) {
						selected.push(s);
					}
				});
				return selected.length ? selected.join(', ') : 'Not set';
			};
		});

//		Check for duplicates and sort
		$scope.genres = $scope.genres.reduce(function(a,b){
			if (a.indexOf(b) < 0 ) a.push(b);
			return a;
		},[]).sort();

		// get extended metadata that are allowed to be displayed
		extendedMetadataService.metadataList().success(function(response) {
			$scope.extendedMetadata = response;
		});

		// filter deleted metadata
		$scope.filterDeletedMetadata = function(metadata) {
			return metadata.isDeleted !== true;
		};

		// filter hidden metadata
		$scope.filterMetadata = function(metadata) {
			return metadata.master_asset_extended_metadata_cd !== "video_flags" &&
			metadata.master_asset_extended_metadata_cd !== "audio_flags" &&
			metadata.master_asset_extended_metadata_cd !== "is_location_chk_reqd" &&
			metadata.master_asset_extended_metadata_cd !== "is_catchup_enabled" &&
			metadata.master_asset_extended_metadata_cd !== "catchup_duration" ;
		};

		// mark extended metadata value pair as deleted
		$scope.deleteMetadata = function(metadataPair) {
			var filtered = $filter('filter')($scope.assetToEdit.extended_metadata_list, {master_asset_extended_metadata_cd: metadataPair.master_asset_extended_metadata_cd, asset_attribute_value: metadataPair.asset_attribute_value});
			if (filtered.length) {
				filtered[0].isDeleted = true;
			}
		};

		//thumbnails section
		$scope.removeThumbnail = function(thumbnail) {
			$scope.assetToEdit.thumbnails.splice(_.indexOf($scope.assetToEdit.thumbnails, thumbnail), 1);
			$scope.idsOfRemovedThumbnails.push(thumbnail.thumbnail_id);
		}

		$scope.addThumbnailToAsset = function(thumbnailName) {
			if (_.indexOf(_.pluck($scope.assetToEdit.thumbnails, 'thumbnail_name'), thumbnailName.name) != -1) {
				if ($scope.assetToEdit.thumbnails[_.indexOf(_.pluck($scope.assetToEdit.thumbnails, 'thumbnail_name'), thumbnailName.name)].thumbnail_id != null) {
					$scope.idsOfRemovedThumbnails.push($scope.assetToEdit.thumbnails[_.indexOf(_.pluck($scope.assetToEdit.thumbnails, 'thumbnail_name'), thumbnailName.name)].thumbnail_id);
				}
				$scope.assetToEdit.thumbnails.splice(_.indexOf(_.pluck($scope.assetToEdit.thumbnails, 'thumbnail_name'), thumbnailName.name), 1);
			}
			if(typeof $scope.assetToEdit.thumbnails != "undefined" && $scope.assetToEdit.thumbnails != null && $scope.assetToEdit.thumbnails.length > 0){
				$scope.assetToEdit.thumbnails.push({thumbnail_name: thumbnailName.name, thumbnail_aspect_ratio: $scope.newFile.width + "x" + $scope.newFile.height, thumbnail_filename: $scope.newFile.name, thumbnail_id: null, size: $scope.newFile.size, format: $scope.newFile.type, thumbnail_url: $scope.dataUrls[0]});
			} else {
				$scope.assetToEdit.thumbnails = [{thumbnail_name: thumbnailName.name, thumbnail_aspect_ratio: $scope.newFile.width + "x" + $scope.newFile.height, thumbnail_filename: $scope.newFile.name, thumbnail_id: null, size: $scope.newFile.size, format: $scope.newFile.type, thumbnail_url: $scope.dataUrls[0]}];
			}
			// use jquery to reset the "file chosen" value in the jquery-controlled input field
			$("#inputFilename")[0].value = "";
		}

		$scope.fileReaderSupported = window.FileReader != null;
		$scope.uploadRightAway = false;
		$scope.selectedFiles = [];

		$scope.onFileSelect = function($files, url) {
			$scope.upload = [];
			$scope.uploadResult = [];
			if ($files[0].type.indexOf('image') === -1) {alert('only images are allows'); }
			$scope.selectedFiles.push($files);
			$scope.dataUrls = [];
			var $file = $files[0];
			$scope.newFile = $file;
			if (window.FileReader && $file.type.indexOf('image') > -1) {
				var fileReader = new FileReader();
				fileReader.readAsDataURL($files[0]);
				var loadFile = function(fileReader, index) {
					fileReader.onload = function(e) {
						var image = new Image();
						image.onload = function(e) {
							$scope.newFile.width = this.width;
							$scope.newFile.height = this.height;
							$scope.selectedFiles[$scope.selectedFiles.length-1].width = this.width;
							$scope.selectedFiles[$scope.selectedFiles.length-1].height = this.height;
						};
						image.src = e.target.result; 
						$timeout(function() {
							$scope.dataUrls[index] = e.target.result;
						});
					}
				}(fileReader, 0);
			}
		};

		$scope.resetInputFile = function() {
			var elems = document.getElementsByTagName('input');
			for (var i = 0; i < elems.length; i++) {
				if (elems[i].type == 'file') {
					elems[i].value = null;
				}
			}
		};

		$scope.showTVSeries = function() {
			var selected = $filter('filter')($scope.modalAssets, {assetName: $scope.assetToEdit.series_name});
			return ($scope.assetToEdit.series_name && selected.length) ? selected[0].series_name : 'Not set';
		};


		// add metadata
		$scope.addNewMetadataRow = function () {
			//
			$scope.inserted = {
					master_asset_extended_metadata_cd: null,
					asset_attribute_value: null,
					asset_attribute_solrfield_name: null,
					asset_attribute_data_type: null
			};
			if ((typeof $scope.assetToEdit.extended_metadata_list != "undefined") && ($scope.assetToEdit.extended_metadata_list.length != 0)) {
				$scope.assetToEdit.extended_metadata_list.push($scope.inserted);
			} else {
				$scope.assetToEdit.extended_metadata_list = [$scope.inserted];
			}
		};


		//
		$scope.checkData = function(data, field){
			//	console.log(field, data);
			switch (field) {
			case 'title':
				$scope.titleDuringEdit = data;
				break;
			case 'network':
				$scope.networkDuringEdit = data;
				break;
			case 'seriesName':
				$scope.seriesNameDuringEdit = data;
				break;
			case 'season':
				$scope.seasonDuringEdit = data;
				break;
			case 'episode':
				$scope.episodeDuringEdit = data;
				break;
			case 'startDate':
				$scope.startDateDuringEdit = data;
				break;
			case 'startTime':
				$scope.startTimeDuringEdit = data;
				break;
			case 'expiresDate':
				$scope.expiresDateDuringEdit = data;
				break;
			case 'expiresTime':
				$scope.expiresTimeDuringEdit = data;
				break;
			}
		}

		$scope.today = new Date();
		$scope.minDate = new Date();

		$scope.clientTimezone = new Date().toString().match(/\(([A-Za-z\s].*)\)/)[1];

		$scope.getCurrentDate = function() {
			return $scope.today;
		}

		$scope.getMinDate = function() {
			return $scope.minDate;
		}

		$scope.correctStartOfAvailability = function(){
			if($scope.startDateDuringEdit == null){
				$scope.assetToEdit.start_of_availability = null;
			} else {
				if ($scope.startTimeDuringEdit == null) {
					var correctDate = null;
					var correctTime = null;
					// if the user used the datepicker
					// returns date object
					if (typeof($scope.startDateDuringEdit) == "object")  {
						correctDate =$scope.startDateDuringEdit.toISOString();
					} else { // returns string
						correctDate = $scope.startDateDuringEdit;
					}
					correctDate = correctDate.split("T")[0];
					// if user used time picker
					// returns date object
					correctTime = new Date().toISOString();
					correctTime = correctTime.split("T")[1];
					correctDate = correctDate.concat("T", correctTime);
					$scope.assetToEdit.start_of_availability = correctDate;
				} else {
					var correctDate = null;
					var correctTime = null;
					// if the user used the datepicker
					// returns date object
					if (typeof($scope.startDateDuringEdit) == "object")  {
						correctDate =$scope.startDateDuringEdit.toISOString();
					} else { // returns string
						correctDate = $scope.startDateDuringEdit;
					}
					correctDate = correctDate.split("T")[0];

					// if user used time picker
					// returns date object
					if (typeof($scope.startTimeDuringEdit) == "object")     {
						correctTime = $scope.startTimeDuringEdit.toISOString();
					} else {
						correctTime = $scope.startTimeDuringEdit;
					}
					correctTime = correctTime.split("T")[1];
					correctDate = correctDate.concat("T", correctTime);
					$scope.assetToEdit.start_of_availability = correctDate;
				}
			}
		}

		$scope.correctExpires = function(){
			if ($scope.expiresDateDuringEdit == null) {
				$scope.assetToEdit.expires = null;
			} else {
				if ($scope.expiresTimeDuringEdit == null) {
					var correctDate = null;
					var correctTime = null;
					if (typeof($scope.expiresDateDuringEdit) == "object")  {
						correctDate =$scope.expiresDateDuringEdit.toISOString();
					} else {
						correctDate = $scope.expiresDateDuringEdit;
					}
					correctDate = correctDate.split("T")[0];
					correctTime = new Date().toISOString();
					correctTime = correctTime.split("T")[1];
					correctDate = correctDate.concat("T", correctTime);
					$scope.assetToEdit.expires = correctDate;

				} else {
					var correctDate = null;
					var correctTime = null;
					if (typeof($scope.expiresDateDuringEdit) == "object")  {
						correctDate =$scope.expiresDateDuringEdit.toISOString();
					} else {
						correctDate = $scope.expiresDateDuringEdit;
					}
					correctDate = correctDate.split("T")[0];

					if (typeof($scope.expiresTimeDuringEdit) == "object")     {
						correctTime = $scope.expiresTimeDuringEdit.toISOString();
					} else {
						correctTime = $scope.expiresTimeDuringEdit;
					}
					correctTime = correctTime.split("T")[1];
					correctDate = correctDate.concat("T", correctTime);
					$scope.assetToEdit.expires = correctDate;
				}
			}
		}

		// this function gets the return object from a thumbnail update
		// and update the updateToEdit with the new thumbnail details
		// in order to refresh the list.

		var setNewImage = function(asset, newThumbnail){
			if (asset.thumbnails == null){
				asset.thumbnails = newThumbnail;
			} else {
				asset.thumbnails[_.indexOf(_.pluck($scope.assetToEdit.thumbnails, 'thumbnail_name'), newThumbnail.thumbnail_name)].thumbnail_id = newThumbnail.thumbnail_id;
			}
		}

		$scope.checkForDuplicateExtendedMetadata = function() {
			if ((typeof($scope.assetToEdit.extended_metadata_list) != 'undefined')) { 
				var x = _.uniq($scope.assetToEdit.extended_metadata_list,false,function(i){  
					return i.master_asset_extended_metadata_cd;
				});  
				if (x.length == $scope.assetToEdit.extended_metadata_list.length) {
					return '';
				} else {
					return "You have added duplicate extended metadata key/value pair.  Please correct and reasubmit";
				}
			} else {
				return '';
			}

		}


		// it will pass the result ($scope.assetToEdit) to modalInstance.result.then
		$scope.validateAndPost = function () {
			var duplicateExtendedMedata = $scope.checkForDuplicateExtendedMetadata();
			if (duplicateExtendedMedata != '') {
				$scope.alerts.push({ type: 'danger', msg: duplicateExtendedMedata} );
				return duplicateExtendedMedata;
			}
			// using cancel because we are not using a copy of asset
			// but the actual asset itself
			// close would destroy asset in the
			//	console.log($scope.assetToEdit);
			if ($scope.assetToEdit.asset_type != 'series') {
				$scope.correctStartOfAvailability();
				$scope.correctExpires();
			}
			// take care of extended metadata
			$scope.saveAsset();
			var jsonAsset = deangularizeAssetFilter($scope.assetToEdit);
			if ((typeof($scope.selectedFiles) == 'undefined') && (typeof($scope.idsOfRemovedThumbnails) == 'undefined')) {   // if no thumbnail updated was done by user
				return Asset.put(jsonAsset)
				.success(function(returnedAsset) {
					if (typeof(returnedAsset[4]) == 'undefined') {
						$modalInstance.close(returnedAsset);
					} else {
						window.location.reload(true);
//						$modalInstance.close('expired');
					}
				}).
				error(function(data, status, headers, config) {
					console.log(data);
					console.log(status);
					$scope.alerts.push({ type: 'danger', msg: status + ': ' + data.error_code + ': ' + data.error_message} );
				});
			} else { // if at least one thumbnail was added and/or deleted

				// 1. remove deleted thumbnails

				$scope.idsOfRemovedThumbnails.forEach( function(thumbnail_id) {
					Thumbnail.remove(thumbnail_id)
					.success(function() {
					}).
					error(function(data, status, headers, config) {
						console.log(data);
						console.log(status);
						$scope.frontPageAlerts.push({ type: 'danger', msg: status + ': ' + data.error_code + ': ' + data.error_message} );
					});
				} );

				// 2. add new ones
				var uploadPromises = [];
				for (var index=0; index < $scope.selectedFiles.length; index++) {
					if (typeof $scope.assetToEdit.thumbnails[_.indexOf(_.pluck($scope.assetToEdit.thumbnails, 'thumbnail_filename'), $scope.selectedFiles[index][0].name)] != "undefined") {
						var thumbnailName = $scope.assetToEdit.thumbnails[_.indexOf(_.pluck($scope.assetToEdit.thumbnails, 'thumbnail_filename'), $scope.selectedFiles[index][0].name)].thumbnail_name;

						var url = 'rest/admin/v5/content/thumbnail?asset_id=' + $scope.assetToEdit.asset_id + '&thumbnail_name=' + thumbnailName + '&aspect_ratio=' + $scope.selectedFiles[index].width + 'x' + $scope.selectedFiles[index].height;
						var promise = $upload.upload({
							url : url,
							method: 'POST',
							file: $scope.selectedFiles[index]
						}).then(function(response) {
							$scope.uploadResult.push(response.data);
							setNewImage($scope.assetToEdit, response.data);
							// after async image update, update asset and close modal
						}, function(response) {
							console.log(response);
							console.log(response.status);
							if (response.status > 0) {
//								$scope.alerts.push({ type: 'danger', msg: response.status + ' ' + response.statusText + ' ' + response.data.error_code + ' ' + response.data.error_message + ' ' + response.data.error_detail + "Please use png/jpg images less than 1MB" } );
//								$scope.editableForm.$setError(err.field, err.msg);
							}
						}, function(evt) {
							// Math.min is to fix IE which reports 200% sometimes
							//$scope.progress[index] = Math.min(100, parseInt(100.0 * evt.loaded / evt.total));
						});
						uploadPromises.push(promise);
					}
				}

				// 3. on success of deletes and posts update the asset and asset thumbnails
				if(typeof $scope.assetToEdit.thumbnails != "undefined" && $scope.assetToEdit.thumbnails != null && $scope.assetToEdit.thumbnails.length > 0){
					for (var i=0; i<$scope.assetToEdit.thumbnails.length; i++) {
						delete $scope.assetToEdit.thumbnails[i].thumbnail_filename;
						delete $scope.assetToEdit.thumbnails[i].thumbnail_url;
					}
				}
				if(typeof uploadPromises != "undefined" && uploadPromises != null && uploadPromises.length > 0){
					$q.all(uploadPromises).then(function (results) {
						var jsonAsset = deangularizeAssetFilter($scope.assetToEdit);
						return Asset.put(jsonAsset).success(function(returnedAsset) {
							if (typeof(returnedAsset[4]) == 'undefined') {
								$modalInstance.close(returnedAsset);
							} else {
								window.location.reload(true);
//								$modalInstance.close('expired');
							}
						}).error(function(data, status, headers, config) {
							console.log(data);
							console.log(status);
							$scope.alerts.push({ type: 'danger', msg: status + ': ' + data.error_code + ': ' + data.error_message} );
						});
					});
				} else {
					return Asset.put(jsonAsset).success(function(returnedAsset) {
						if (typeof(returnedAsset[4]) == 'undefined') {
							$modalInstance.close(returnedAsset);
						} else {
							window.location.reload(true);
//							$modalInstance.close('expired');
						}
					}).error(function(data, status, headers, config) {
						console.log(data);
						console.log(status);
						$scope.alerts.push({ type: 'danger', msg: status + ': ' + data.error_code + ': ' + data.error_message} );
					});
				}
			}
		};

		// Cancel method for the x-editable form
		$scope.cancelForm = function(){
			$modalInstance.close();
		}

//		Retry for the modal
		$scope.retry = function () {
			$scope.editableForm.$show();
			$scope.alerts = [];

		};

		$scope.saveAsset = function () {
			if (typeof $scope.assetToEdit.extended_metadata_list != "undefined") {
				for (var i = $scope.assetToEdit.extended_metadata_list.length; i--;) {
					var metadataTuple = $scope.assetToEdit.extended_metadata_list[i];
					// actually delete asset
					if (metadataTuple.isDeleted) {
						$scope.assetToEdit.extended_metadata_list.splice(i, 1);
					}
				}
			}
			if ((typeof $scope.assetToEdit.extended_metadata_list == "undefined") || ($scope.assetToEdit.extended_metadata_list.length == 0)) {
				delete $scope.assetToEdit.extended_metadata_list;
			}
		};

	}]); //controller end
});