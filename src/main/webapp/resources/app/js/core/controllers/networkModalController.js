define(['controllers/controllers', 'services/damServices', 'filters/formatFileSize'],
    function(controllers) {
        'use strict';
        controllers.controller('NetworkModalCtrl', ['$scope', '$rootScope', 'networkToEdit', '$modalInstance', '$upload', '$q', '$timeout', 'Network', 'Networks', 'Thumbnail', 'thumbnailTypesService', function ($scope, $rootScope, networkToEdit, $modalInstance, $upload, $q, $timeout, Network, Networks, Thumbnail, thumbnailTypesService, $http, $log, $resource, $filter) {
        	$scope.networkToEdit = networkToEdit;
            $scope.thumbnailTypes = [{"name":"","display_name":"-- Choose Thumbnail Type --"}];
            $scope.thumbnail_name = $scope.thumbnailTypes[0];
            $scope.alerts = [];
            $scope.idsOfRemovedThumbnails = [];
            $scope.primary_thumbnail_id = null;
            $scope.fileReaderSupported = window.FileReader != null;
            $scope.uploadRightAway = false;
            $scope.selectedFiles = [];
            
            $scope.closeAlert = function(index) {
                $scope.alerts.splice(index, 1);
            };

            angular.forEach($scope.networkToEdit.network_logos, function (s) {
                if (s.thumbnail_name == 'primary') {
                	$scope.primary_thumbnail_id = s.thumbnail_id;
                }
            });
            
            //thumbnails section
            $scope.removeThumbnail = function(thumbnail) {
                $scope.networkToEdit.network_logos.splice(_.indexOf($scope.networkToEdit.network_logos, thumbnail), 1);
                $scope.idsOfRemovedThumbnails.push(thumbnail.thumbnail_id);
            }

            $scope.addThumbnailToNetwork = function(thumbnailName) {
                if (_.indexOf(_.pluck($scope.networkToEdit.network_logos, 'thumbnail_name'), thumbnailName.name) != -1) {
                    if ($scope.networkToEdit.network_logos[_.indexOf(_.pluck($scope.networkToEdit.network_logos, 'thumbnail_name'), thumbnailName.name)].thumbnail_id != null) {
                        $scope.idsOfRemovedThumbnails.push($scope.networkToEdit.network_logos[_.indexOf(_.pluck($scope.networkToEdit.network_logos, 'thumbnail_name'), thumbnailName.name)].thumbnail_id);
                    }
                    $scope.networkToEdit.network_logos.splice(_.indexOf(_.pluck($scope.networkToEdit.network_logos, 'thumbnail_name'), thumbnailName.name), 1);
                }
                if(typeof $scope.networkToEdit.network_logos != "undefined" && $scope.networkToEdit.network_logos != null && $scope.networkToEdit.network_logos.length > 0){
                    $scope.networkToEdit.network_logos.push({thumbnail_name: thumbnailName.name, thumbnail_aspect_ratio: $scope.newFile.width + "x" + $scope.newFile.height, thumbnail_filename: $scope.newFile.name, thumbnail_id: null, size: $scope.newFile.size, format: $scope.newFile.type, thumbnail_url: $scope.dataUrls[0]});
                } else {
                    $scope.networkToEdit.network_logos = [{thumbnail_name: thumbnailName.name, thumbnail_aspect_ratio: $scope.newFile.width + "x" + $scope.newFile.height, thumbnail_filename: $scope.newFile.name, thumbnail_id: null, size: $scope.newFile.size, format: $scope.newFile.type, thumbnail_url: $scope.dataUrls[0]}];
                }
                // use jquery to reset the "file chosen" value in the jquery-controlled input field
                $("#inputFilename")[0].value = "";
            }
            
            // Get Thumbnail Types
            thumbnailTypesService.controlledThumbnailTypes().success(function(response) {
                $scope.thumbnailTypes = $scope.thumbnailTypes.concat(response.vocabulary);
            });

            $scope.onFileSelect = function($files, url) {
                $scope.upload = [];
                $scope.uploadResult = [];
                if ($files[0].type.indexOf('image') === -1) {alert('only images are allows'); }
                $scope.selectedFiles.push($files);
                $scope.dataUrls = [];
                var $file = $files[0];
                $scope.newFile = $file;
                if (window.FileReader && $file.type.indexOf('image') > -1) {
                    var fileReader = new FileReader();
                    fileReader.readAsDataURL($files[0]);
                    var loadFile = function(fileReader, index) {
                        fileReader.onload = function(e) {
                            var image = new Image();
                            image.onload = function(e) {
                                $scope.newFile.width = this.width;
                                $scope.newFile.height = this.height;
                                $scope.selectedFiles[$scope.selectedFiles.length-1].width = this.width;
                                $scope.selectedFiles[$scope.selectedFiles.length-1].height = this.height;
                            };
                            image.src = e.target.result; 
                            $timeout(function() {
                                $scope.dataUrls[index] = e.target.result;
                            });
                        }
                    }(fileReader, 0);
                }
                // Do not upload automatically on file selection
                //				$scope.uploadThumbnail(0, url);
            };

            $scope.resetInputFile = function() {
                var elems = document.getElementsByTagName('input');
                for (var i = 0; i < elems.length; i++) {
                    if (elems[i].type == 'file') {
                        elems[i].value = null;
                    }
                }
            };              
            
            // this function gets the return object from a thumbnail update
            // and update the updateToEdit with the new thumbnail details
            // in order to refresh the list.

            var setNewImage = function(network, newThumbnail){
                if (network.network_logos == null){
                	network.network_logos = newThumbnail;
                } else {
                	network.network_logos[_.indexOf(_.pluck($scope.networkToEdit.network_logos, 'thumbnail_name'), newThumbnail.thumbnail_name)].thumbnail_id = newThumbnail.thumbnail_id;
                }
            }
            
            // it will pass the result ($scope.networkToEdit) to modalInstance.result.then
            $scope.ok = function () {
                // using cancel because we are not using a copy of network
                // but the actual network itself
                // close would destroy network in the
                // console.log($scope.networkToEdit);
            	
                if ((typeof($scope.selectedFiles) == 'undefined') && (typeof($scope.idsOfRemovedThumbnails) == 'undefined')) {   // if no thumbnail updated was done by user
                   return Network.update($scope.networkToEdit)
                        .success(function(returnedNetwork) {
                            if (typeof(returnedNetwork[4]) == 'undefined') {
                                $modalInstance.close(returnedNetwork);
                            } else {
                                window.location.reload(true);
//          	    $modalInstance.close('expired');
                            }
                        }).
                        error(function(data, status, headers, config) {
                            console.log(data);
                            console.log(status);
                            $scope.alerts.push({ type: 'danger', msg: status + ': ' + data.error_code + ': ' + data.error_message} );
                        });
                } else { // if at least one thumbnail was added and/or deleted

                    // 1. remove deleted thumbnails

                    $scope.idsOfRemovedThumbnails.forEach( function(thumbnail_id) {
                        Thumbnail.remove(thumbnail_id)
                            .success(function() {
                            }).
                            error(function(data, status, headers, config) {
                                console.log(data);
                                console.log(status);
                                $scope.frontPageAlerts.push({ type: 'danger', msg: status + ': ' + data.error_code + ': ' + data.error_message} );
                            });
                    } );

                    // 2. add new ones
                    var uploadPromises = [];
                    for (var index=0; index < $scope.selectedFiles.length; index++) {
                   	 if (typeof $scope.networkToEdit.network_logos[_.indexOf(_.pluck($scope.networkToEdit.network_logos, 'thumbnail_filename'), $scope.selectedFiles[index][0].name)] != "undefined") {
                            var thumbnailName = $scope.networkToEdit.network_logos[_.indexOf(_.pluck($scope.networkToEdit.network_logos, 'thumbnail_filename'), $scope.selectedFiles[index][0].name)].thumbnail_name;

                            var specialCharsFlag = false;
                            var specialChars = " _!+&Éé\\-";
                            for(i = 0; i < specialChars.length;i++){
	                            if($scope.networkToEdit.network_cd.indexOf(specialChars[i]) > -1){
	                            		var encodedNetwork = encodeURIComponent($scope.networkToEdit.network_cd);
                                     var url = 'rest/admin/v5/content/thumbnail?network_cd=' + encodedNetwork+ '&thumbnail_name=' + thumbnailName + '&aspect_ratio=' + $scope.selectedFiles[index].width + 'x' + $scope.selectedFiles[index].height;
                                     specialCharsFlag = true;
                                     break;
	                            }
	                        }
	                        if(specialCharsFlag == false) {
	                                 var url = 'rest/admin/v5/content/thumbnail?network_cd=' + $scope.networkToEdit.network_cd + '&thumbnail_name=' + thumbnailName + '&aspect_ratio=' + $scope.selectedFiles[index].width + 'x' + $scope.selectedFiles[index].height;                                
	                        }
                        
                            var promise = $upload.upload({
                            url : url,
                            method: 'POST',
                            file: $scope.selectedFiles[index]
                        }).then(function(response) {
                                $scope.uploadResult.push(response.data);
                                setNewImage($scope.networkToEdit, response.data);
                                // after async image update, update network and close modal
                            }, function(response) {
                                console.log(response);
                                console.log(response.status);
                                if (response.status > 0) {
//                                $scope.alerts.push({ type: 'danger', msg: response.status + ' ' + response.statusText + ' ' + response.data.error_code + ' ' + response.data.error_message + ' ' + response.data.error_detail + "Please use png/jpg images less than 1MB" } );
//                                $scope.editableForm.$setError(err.field, err.msg);
                                }
                            }, function(evt) {
                                // Math.min is to fix IE which reports 200% sometimes
                                //$scope.progress[index] = Math.min(100, parseInt(100.0 * evt.loaded / evt.total));
                            });
                        uploadPromises.push(promise);
                   	 }
                    }
                    // 3. on success of deletes and posts update the network and network thumbnails
                    if(typeof $scope.networkToEdit.network_logos != "undefined" && $scope.networkToEdit.network_logos != null && $scope.networkToEdit.network_logos.length > 0){
                        for (var i=0; i<$scope.networkToEdit.network_logos.length; i++) {
                            delete $scope.networkToEdit.network_logos[i].thumbnail_filename;
                            delete $scope.networkToEdit.network_logos[i].thumbnail_url;
                        }
                    }
                    if(typeof uploadPromises != "undefined" && uploadPromises != null && uploadPromises.length > 0){
                        $q.all(uploadPromises).then(function (results) {
                                    return Network.update($scope.networkToEdit).success(function(returnedNetwork) {
                                if (typeof(returnedNetwork[4]) == 'undefined') {
                                    $modalInstance.close(returnedNetwork);
                                } else {
                                    window.location.reload(true);
//           	    $modalInstance.close('expired');
                                }
                            }).error(function(data, status, headers, config) {
                                    console.log(data);
                                    console.log(status);
                                    $scope.alerts.push({ type: 'danger', msg: status + ': ' + data.error_code + ': ' + data.error_message} );
                                });
                        });
                    } else {
                        return Network.update($scope.networkToEdit).success(function(returnedNetwork) {
                            if (typeof(returnedNetwork[4]) == 'undefined') {
                                $modalInstance.close(returnedNetwork);
                            } else {
                                window.location.reload(true);
//               	    $modalInstance.close('expired');
                            }
                        }).error(function(data, status, headers, config) {
                                console.log(data);
                                console.log(status);
                                $scope.alerts.push({ type: 'danger', msg: status + ': ' + data.error_code + ': ' + data.error_message} );
                            });
                    }
                }
            };

            // Cancel method for the x-editable form
            $scope.cancelForm = function(){
                $modalInstance.close();
            }


//  Retry for the modal
            $scope.retry = function () {
                $scope.editableForm.$show();
                $scope.alerts = [];

            };

        


        }]); //controller end
});