/**
 * Created by kliolios on 2014-06-03.
 */
define(['controllers/controllers', 'controllers/networkModalController', 'controllers/assetModalController', 'filters/angularize', 'filters/deAngularize', 'services/cvServices', 'services/ldapServices', 'services/damServices', 'filters/formatFileSize'],
    function(controllers) {
        'use strict';
        controllers.controller('ContentProviderController', ['$scope', '$rootScope', '$controller', '$http', '$modal', '$log', '$resource', '$filter', '$q', 'LoggedUserDetails', 'assetStateService', 'Thumbnail', 'Network', 'Networks', 'Assets', 'Asset', 'ContentProviders', function ($scope, $rootScope, $controller, $http, $modal, $log, $resource, $filter, $q, LoggedUserDetails, assetStateService, Thumbnail, Network, Networks, Assets, Asset, ContentProviders) {
                //initialization of global variables
                $scope.selectedAssetState = '';
                $scope.selectedAssetStateName = 'All';
                $scope.frontPageAlerts = [];
                $scope.episodeOffset = 0;
                $scope.seriesOffset = 0;
                $scope.pageSizes=[{id:10,name:"10"},{id:50,name:"50"},{id:100,name:"100"},{id:500,name:"500"}];
                $scope.currentEpisodePage = {};
                $scope.currentSeriesPage = {};
                $scope.currentEpisodePage.num = 1;
                $scope.currentSeriesPage.num = 1;
                $scope.selectedIndex = -1;
                $scope.assetTypeEpisode = 'vod';
                $scope.assetTypeSeries = 'series';
                $scope.seriesAssets = [];
                $scope.episodeAssets = [];
                $scope.episodeItemsPerPage = {};
                $scope.episodeItemsPerPage.selected = $scope.pageSizes[0];
                $scope.seriesItemsPerPage = {};
                $scope.seriesItemsPerPage.selected = $scope.pageSizes[0];
                $scope.maxPageSize = 5;
                $scope.searchString = "";
                $scope.Header = ['','glyphicon glyphicon-chevron-down'];
                $scope.iconName = 'glyphicon glyphicon-chevron-down';
                $scope.sort_by = 'last_update_time';
                $scope.sort_direction = 'desc';
                
                $scope.resetAll = function()
                {
                    $scope.Header = ['',''];
                    $scope.sort_direction = 'desc';                	
                }
                
                $scope.sortEpisodes = function(sort_by){
                    $scope.sort_by = sort_by;

                    if($scope.sort_direction == 'asc') {
                        $scope.sort_direction = 'desc';        
                        $scope.iconName = 'glyphicon glyphicon-chevron-down';        	
                    } else {
                        $scope.sort_direction = 'asc';                	
                        $scope.iconName = 'glyphicon glyphicon-chevron-up';
                    }
                    
                    if(sort_by === 'name')
                    {
                        $scope.Header[0] = $scope.iconName;
                        $scope.Header[1] = '';
                    }
                    else if(sort_by === 'last_update_time')
                    {
                        $scope.Header[1] = $scope.iconName;
                        $scope.Header[0] = '';
                    }
                    Assets.get({
            			type:$scope.assetTypeEpisode, 
            			provider_id:  $scope.selectedContentProvider,
            			series_id: $scope.selectedSeries,
            			offset:0,
            			length:$scope.episodeItemsPerPage.selected.id,
            			sort_by: sort_by,
            			sort_direction: $scope.sort_direction
            				
            	})
                    .success(function(data, status, headers, config) {
                        $scope.episodeAssets = data.assets;
                        $scope.episodeTotal = data.total;
                        if ($scope.episodeAssets == null) {
                            $scope.episodeAssets = [];
                        }
                    })
                    .error(function(data, status, headers, config) {
                        console.log(data);
                        console.log(status);
                        $scope.frontPageAlerts.push({ type: 'danger', msg: data.error_code + ': ' + data.error_message, mobi: 'The DAM API is currently unavailable.  Please try again later. ' + data.error_detail} );
                    });	
                }
                
                $scope.searchEpisodes = function(searchString){
                	$scope.currentEpisodePage.num = 1;
                    Assets.get({type:$scope.assetTypeEpisode, provider_id:$scope.selectedContentProvider, offset:0, length:$scope.episodeItemsPerPage.selected.id, name:searchString})
                        .success(function(data, status, headers, config) {
                            $scope.episodeAssets = data.assets;
                            $scope.episodeTotal = data.total;
                            if ($scope.episodeAssets == null) {
                                $scope.episodeAssets = [];
                            }
                        })
                        .error(function(data, status, headers, config) {
                            console.log(data);
                            console.log(status);
                            $scope.frontPageAlerts.push({ type: 'danger', msg: data.error_code + ': ' + data.error_message, mobi: 'The DAM API is currently unavailable.  Please try again later. ' + data.error_detail} );
                        });	
                }
                
                $scope.getPaginatedEpisodes = function() {
                	$scope.currentEpisodePage.num = 1;
                    Assets.get({type:$scope.assetTypeEpisode, provider_id:$scope.selectedContentProvider, offset:0, length:$scope.episodeItemsPerPage.selected.id})
                        .success(function(data, status, headers, config) {
                            $scope.episodeAssets = data.assets;
                            $scope.episodeTotal = data.total;
                            if ($scope.episodeAssets == null) {
                                $scope.episodeAssets = [];
                            }
                        })
                        .error(function(data, status, headers, config) {
                            console.log(data);
                            console.log(status);
                            $scope.frontPageAlerts.push({ type: 'danger', msg: data.error_code + ': ' + data.error_message, mobi: 'The DAM API is currently unavailable.  Please try again later. ' + data.error_detail} );
                        });

                }

                $scope.getPaginatedSeries = function() {
                    $scope.currentSeriesPage.num = 1;
                    Assets.get($scope.assetTypeSeries, $scope.selectedContentProvider, 0, $scope.seriesItemsPerPage.selected.id)
                        .success(function(data, status, headers, config) {
                            $scope.seriesAssets  = data.assets;
                            $scope.seriesTotal = data.total;
                            if ($scope.seriesAssets == null) {
                                $scope.seriesAssets = [];
                            }
                            $rootScope.seriesAssets = $scope.seriesAssets;
                        })
                        .error(function(data, status, headers, config) {
                            console.log(data);
                            console.log(status);
                            $scope.frontPageAlerts.push({ type: 'danger', msg: data.error_code + ': ' + data.error_message, mobi: 'The DAM API is currently unavailable.  Please try again later. ' + data.error_detail} );
                        });

                }

                LoggedUserDetails.get().
                    success(function(data, status, headers, config) {
                        $scope.loggedInUserDetails = data;
                        $scope.isNetworkAdmin = function() {
                            var isNetworkAdmin = false;
                            angular.forEach($scope.loggedInUserDetails.authorities, function (authority) {
                                if (authority.authority == 'ROLE_NETWORK_ADMIN') {
                                    isNetworkAdmin = true;
                                }
                            });
                            return isNetworkAdmin;
                        };
                    }).
                    error(function(data, status, headers, config) {
                        console.log(data);
                        console.log(status);
                        $scope.frontPageAlerts.push({ type: 'danger', msg: status + ': ' + data.error_code + ': ' + data.error_message + ' The DAM API is currently unavailable.  Please try again later.' } );
                    });


                $scope.closeAlert = function(index) {
                    $scope.frontPageAlerts.splice(index, 1);
                };

                Networks.get()
                    .success(function(data, status, headers, config) {
                        $scope.networks = data.networks;
                    })
                    .error(function(data, status, headers, config) {
                        $scope.frontPageAlerts.push({ type: 'danger', msg: status + ': ' + data.error_code + ': ' + data.error_message + ' The DAM API is currently unavailable.  Please try again later.' } );
                    });

                ContentProviders.get()
                    .success(function(data, status, headers, config) {
                        $scope.contentProviders = data;
                        $scope.selectedContentProvider = data[0].providerId;
                        $scope.selectedContentProviderName = data[0].providerName;

                        // get episodes
                        Assets.get({
                			type:$scope.assetTypeEpisode, 
                			provider_id:  $scope.selectedContentProvider,
                			offset:$scope.episodeOffset,
                			length:$scope.episodeItemsPerPage.selected.id
                				
                        }).success(function(data, status, headers, config) {
                                $scope.episodeAssets = data.assets;
                                $scope.episodeTotal = data.total;
                                if ($scope.episodeAssets == null) {
                                    $scope.episodeAssets = [];
                                }
                            })
                            .error(function(data, status, headers, config) {
                                console.log(data);
                                console.log(status);
                                $scope.frontPageAlerts.push({ type: 'danger', msg: data.error_code + ': ' + data.error_message, mobi: 'The DAM API is currently unavailable.  Please try again later. ' + data.error_detail} );
                            });

                        // get series
                        Assets.get( {
                			type:$scope.assetTypeSeries, 
                			provider_id:  $scope.selectedContentProvider,
                			offset:$scope.seriesOffset,
                			length:$scope.seriesItemsPerPage.selected.id
                				
                	})
                            .success(function(data, status, headers, config) {
                                $scope.seriesAssets  = data.assets;
                                $scope.seriesTotal = data.total;
                                if ($scope.seriesAssets == null) {
                                    $scope.seriesAssets = [];
                                }
                                $rootScope.seriesAssets = $scope.seriesAssets;
                            })
                            .error(function(data, status, headers, config) {
                                console.log(data);
                                console.log(status);
                                $scope.frontPageAlerts.push({ type: 'danger', msg: data.error_code + ': ' + data.error_message, mobi: 'The DAM API is currently unavailable.  Please try again later. ' + data.error_detail} );
                            });
                    })
                    .error(function(data, status, headers, config) {
                        console.log(data);
                        console.log(status);
                        $scope.frontPageAlerts.push({ type: 'danger', msg: data.error_code + ': ' + data.error_message, mobi: 'LDAP is currently unavailable.  Please try again later. ' + data.error_detail});
                    });

                assetStateService.controlledAssetStates().success(function(response) {
                    $scope.assetStates = response;
                });

                $scope.setSelectedState = function(stateName, stateValue) {
                    $scope.selectedAssetState = stateValue;
                    $scope.selectedAssetStateName = stateName;
                }

                $scope.setSelectedSeries = function (assetId, assetName) {
                    $scope.selectedSeries = assetId;
                    $scope.selectedSeriesName = assetName;
                    // filter by series
                    Assets.get({type:$scope.assetTypeEpisode, provider_id:$scope.selectedContentProvider, offset:$scope.episodeOffset, length:$scope.episodeItemsPerPage.selected.id, series_id:$scope.selectedSeries, sort_direction:$scope.sort_direction, sort_by:$scope.sort_by}).
                        success(function(data, status, headers, config) {
                            $scope.episodeTotal = data.total;
                            $scope.episodeAssets = data.assets;
                            if ($scope.episodeAssets == null) {
                                $scope.episodeAssets = [];
                            }
                        }).
                        error(function(data, status, headers, config) {
                            console.log(data);
                            console.log(status);
                            $scope.frontPageAlerts.push({ type: 'danger', msg: status + ': ' + data.error_code + ': ' + data.error_message + ' The DAM API is currently unavailable.  Please try again later.' } );
                        });
                }


                self.toggleSelect = function(ind){
                    if( ind === self.selectedIndex ){
                        self.selectedIndex = -1;
                    } else{
                        self.selectedIndex = ind;
                    }
                }

                self.getClass = function(ind){
                    if( ind === self.selectedIndex ){
                        return "selected";
                    } else{
                        return "";
                    }
                }

                $scope.selectSeriesPage = function(page){
                    if (page == 1) {
                        $scope.seriesOffset = 0;
                    } else {
                        $scope.seriesOffset = ((page - 1) * $scope.seriesItemsPerPage.selected.id) ;
                    }
                    Assets.get({type:$scope.assetTypeSeries, provider_id:$scope.selectedContentProvider, offset:$scope.seriesOffset, length:$scope.seriesItemsPerPage.selected.id})
                        .success(function(data, status, headers, config) {
                            $scope.seriesTotal = data.total;
                            $scope.seriesAssets = data.assets;
                            if ($scope.seriesAssets == null) {
                                $scope.seriesAssets = [];
                            }
                        })
                        .error(function(data, status, headers, config) {
                            console.log(data);
                            console.log(status);
                            $scope.frontPageAlerts.push({ type: 'danger', msg: status + ': ' + data.error_code + ': ' + data.error_message + ' The DAM API is currently unavailable.  Please try again later.' } );
                        });
                }

                $scope.selectEpisodePage = function(page){
                    if (page == 1) {
                        $scope.episodeOffset = 0;
                    } else {
                        $scope.episodeOffset = ((page - 1) * $scope.episodeItemsPerPage.selected.id) ;
                    }

                    Assets.get({type:$scope.assetTypeEpisode, provider_id:$scope.selectedContentProvider, offset:$scope.episodeOffset, length:$scope.episodeItemsPerPage.selected.id})
                        .success(function(data, status, headers, config) {
                            $scope.episodeTotal = data.total;
                            $scope.episodeAssets = data.assets;
                            if ($scope.episodeAssets == null) {
                                $scope.episodeAssets = [];
                            }
                        })
                        .error(function(data, status, headers, config) {
                            console.log(data);
                            console.log(status);
                            $scope.frontPageAlerts.push({ type: 'danger', msg: status + ': ' + data.error_code + ': ' + data.error_message + ' The DAM API is currently unavailable.  Please try again later.' } );
                        });

                }

                $scope.setSelectedContentProvider = function (providerId, providerName) {
                    delete $scope.selectedSeries;
                    delete $scope.selectedSeriesName;
                    $scope.episodeOffset = 0;
                    $scope.seriesOffset = 0;
                	$scope.currentEpisodePage.num = 1;
                	$scope.currentSeriesPage.num = 1;
                    $scope.episodeItemsPerPage.selected = $scope.pageSizes[0];
                    $scope.seriesItemsPerPage.selected = $scope.pageSizes[0];
                    $scope.selectedContentProvider = providerId;
                    $scope.selectedContentProviderName = providerName;
                    // get episodes
                    Assets.get({type:$scope.assetTypeEpisode, provider_id:$scope.selectedContentProvider, offset:$scope.episodeOffset, length:$scope.seriesItemsPerPage.selected.id}).
                        success(function(data, status, headers, config) {
                            $scope.episodeTotal = data.total;
                            $scope.episodeAssets = data.assets;
                            if ($scope.episodeAssets == null) {
                                $scope.episodeAssets = [];
                            }
                        }).
                        error(function(data, status, headers, config) {
                            console.log(data);
                            console.log(status);
                            $scope.frontPageAlerts.push({ type: 'danger', msg: status + ': ' + data.error_code + ': ' + data.error_message + ' The DAM API is currently unavailable.  Please try again later.' } );
                        });
                    // get series
                    Assets.get({type:$scope.assetTypeSeries, provider_id:$scope.selectedContentProvider, offset:$scope.seriesOffset, length:$scope.seriesItemsPerPage.selected.id}).
                        success(function(data, status, headers, config) {
                            $scope.seriesAssets  = data.assets;
                            $scope.seriesTotal  = data.total;
                            if ($scope.seriesAssets == null) {
                                $scope.seriesAssets = [];
                            }
                            $rootScope.seriesAssets = $scope.seriesAssets;
                        }).
                        error(function(data, status, headers, config) {
                            console.log(data);
                            console.log(status);
                            $scope.frontPageAlerts.push({ type: 'danger', msg: status + ': ' + data.error_code + ': ' + data.error_message + ' The DAM API is currently unavailable.  Please try again later.' } );
                        });
                }

                $scope.compareEpisodeDate = function(expires) {
                    var now = new Date();
                    now.setMinutes(now.getMinutes() + 5);
                    return Boolean(expires != null && expires < now);
                }

                $scope.compareDate = function(expires) {
                    // use valueOf to get epoch
                    var now = new Date().valueOf();
                    return Boolean(expires != null && expires < now);
                }

                $scope.expireNetwork = function(network) {
                    Network.remove(network.network_cd)
                        .success(function() {
                            $scope.networks.splice(_.indexOf($scope.networks, network), 1);
                        }).
                        error(function(data, status, headers, config) {
                            console.log(data);
                            console.log(status);
                            $scope.frontPageAlerts.push({ type: 'danger', msg: status + ': ' + data.error_code + ': ' + data.error_message} );
                        });
                }

                $scope.expireAsset = function(asset) {
                    var now = new Date();
                    if (asset.asset_type != 'series') {
                        now.setMinutes(now.getMinutes() + 5);
                    }
                    var oldExpires = asset.expires;
                    asset.expires = now;
                    Asset.put(asset)
                        .success(function(returnedAsset) {
                            if (asset.assetType == 'series'){
                                $scope.seriesAssets[_.chain($scope.seriesAssets).pluck("asset_id").indexOf(returnedAsset.asset_id).value()] = returnedAsset;
                            } else {
                                $scope.episodeAssets[_.chain($scope.episodeAssets).pluck("asset_id").indexOf(returnedAsset.asset_id).value()] = returnedAsset;
                            }
                        })
                        .error(function(data, status, headers, config) {
                            console.log(data);
                            console.log(status);
                            asset.expires = oldExpires;
                            $scope.frontPageAlerts.push({ type: 'danger', msg: status + ': ' + data.error_code + ': ' + data.error_message} );
                        });
                };

                $scope.resetSelectedSeries = function(){
                    delete $scope.selectedSeries ;
                    delete $scope.selectedSeriesName;
                    Assets.get({type:$scope.assetTypeEpisode, provider_id:$scope.selectedContentProvider, offset:$scope.episodeOffset, length:$scope.seriesItemsPerPage.selected.id}).
                    success(function(data, status, headers, config) {
                        $scope.episodeTotal = data.total;
                        $scope.episodeAssets = data.assets;
                        if ($scope.episodeAssets == null) {
                            $scope.episodeAssets = [];
                        }
                    }).
                    error(function(data, status, headers, config) {
                        console.log(data);
                        console.log(status);
                        $scope.frontPageAlerts.push({ type: 'danger', msg: status + ': ' + data.error_code + ': ' + data.error_message + ' The DAM API is currently unavailable.  Please try again later.' } );
                    });
                    
                }

                // MODAL section


                //it will open a modal service and pass assetToEdit to ModalInstanceCtrl
                $scope.editNetwork = function (network) {
                    var modalInstance = $modal.open({
                        templateUrl: 'resources/app/js/core/ng-templates/editNetwork.html',
                        controller: 'NetworkModalCtrl',
                        windowClass: 'app-modal-window',
                        resolve: {
                            networkToEdit: function () {
                                return network;
                            }
                        }
                    });

                    // gets invoked if you click the close/ok button in the modal.
                    // it does not execute if you click on the background to close the modal
                    modalInstance.result.then(function (networkToEdit) {
                        if (networkToEdit != null){
                            $scope.networks[_.chain($scope.networks).pluck("network_cd").indexOf(networkToEdit.network_cd).value()] = angular.fromJson(networkToEdit);
                        }
                    }, function () {
                        $log.info('Modal dismissed at: ' + new Date());
                    });
                };

                //it will open a modal service and pass assetToEdit to ModalInstanceCtrl
                $scope.editSeries = function (asset) {
                    var modalInstance = $modal.open({
                        templateUrl: 'resources/app/js/core/ng-templates/editSeries.html',
                        controller: 'AssetModalCtrl',
                        windowClass: 'app-modal-window',
                        resolve: {
                            assetToEdit: function () {
                                return asset;
                            }
                        }
                    });

                    // gets invoked if you click the close/ok button in the modal.
                    // it does not execute if you click on the background to close the modal
                    modalInstance.result.then(function (assetToEdit) {
                        if (assetToEdit != null){
                            $scope.seriesAssets[_.chain($scope.seriesAssets).pluck("asset_id").indexOf(assetToEdit.asset_id).value()] = angular.fromJson(assetToEdit);
                        }
                    }, function () {
                        $log.info('Modal dismissed at: ' + new Date());
                    });
                };

                //it will open a modal service and pass assetToEdit to ModalInstanceCtrl
                $scope.editEpisode = function (asset) {
                    var modalInstance = $modal.open({
                        templateUrl: 'resources/app/js/core/ng-templates/editEpisode.html',
                        controller: 'AssetModalCtrl',
                        windowClass: 'app-modal-window',
                        backdrop: 'static',
                        resolve: {
                            assetToEdit: function () {
                                return asset;
                            }
                        }
                    });

                    // gets invoked if you click the close/ok button in the modal.
                    // it does not execute if you click on the background to close the modal
                    modalInstance.result.then(function (assetToEdit) {
                        if (assetToEdit != null){
                            $scope.episodeAssets[_.chain($scope.episodeAssets).pluck("assetId").indexOf(asset.assetId).value()] = angular.fromJson(assetToEdit);
                        }
                    }, function () {
                        $log.info('Modal dismissed at: ' + new Date());
                    });
                };

            }]).run(function(editableOptions) {
                editableOptions.theme = 'bs3'; // bootstrap3 theme. Can be also 'bs2', 'default'
            })
            .factory('_', function () {// underscore dependency
                return window._; // assumes underscore has already been loaded on the page
            }).filter('alternate_asset_id_filter', function() {
                return function(input) {
                    input = input || '';
                    var out = "";
                    if (input == ''){}else{
                        out = input.split('::')[3] + '::' + input.split('::')[4];
                    }
                    return out;
                };
            }).filter('series_alternate_asset_id_filter', function() {
                return function(input) {
                    input = input || '';
                    var out = "";
                    if (input == ''){
                    }else{
                        out = input.split('::')[4];
                        if (typeof(out) == 'undefined'){
                            out = input.split('::')[3];
                        }
                    }
                    return out;
                };
            }).filter('HHMMSS', ['$filter', function ($filter) {
                return function (input, decimals) {
                    var sec_num = parseInt(input, 10),
                        decimal = parseFloat(input) - sec_num,
                        hours   = Math.floor(sec_num / 3600),
                        minutes = Math.floor((sec_num - (hours * 3600)) / 60),
                        seconds = sec_num - (hours * 3600) - (minutes * 60);

                    if (hours   < 10) {hours   = "0"+hours;}
                    if (minutes < 10) {minutes = "0"+minutes;}
                    if (seconds < 10) {seconds = "0"+seconds;}
                    var time    = hours+':'+minutes+':'+seconds;
                    if (decimals > 0) {
                        time += '.' + $filter('number')(decimal, decimals).substr(2);
                    }
                    return time;
                };
            }]); //controller end
    });
