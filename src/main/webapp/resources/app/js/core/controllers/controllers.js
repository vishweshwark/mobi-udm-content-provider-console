/*global define:false */
define(['angular', 'main'], function(angular) {
  'use strict';
  return angular.module('controllers', []);
});