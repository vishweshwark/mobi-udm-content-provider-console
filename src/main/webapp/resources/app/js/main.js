requirejs.config({
	//By default load any module IDs from js/lib
	baseUrl: 'resources/app/',
	//except, if the module ID starts with "app",
	//load it from the js/app directory. paths
	//config is relative to the baseUrl, and
	//never includes a ".js" extension since
	//the paths config could be for a directory.
	paths: {
		jquery: 'lib/jquery',
		angular: 'lib/angular',
		bootstrap: 'lib/bootstrap-3.1.1-dist/js/bootstrap',
		angularFileUploadShim: 'lib/angular-file-upload-bower-1.4.0/angular-file-upload-shim',
		angularFileUpload: 'lib/angular-file-upload-bower-1.4.0/angular-file-upload',
		underscore: 'lib/underscore-min',
		xeditable: 'lib/angular-xeditable/js/xeditable',
		angularRoute: 'lib/angular-route',
		angularResource: 'lib/angular-resource',
		checklistModel: 'lib/checklist-model',
		uiBootstrapTpls: 'lib/ui-bootstrap-tpls-0.10.0.min',
		ngInfiniteScroll: 'lib/ng-infinite-scroll.min',
		filters: 'js/core/filters',
		services: 'js/core/services',
		controllers: 'js/core/controllers'
	},
	shim:{
		angular: {deps: ['jquery'], exports: 'angular'},
		ngInfiniteScroll: {deps: ['jquery','angular'], exports: 'ngInfiniteScroll'},
		angularResource: {deps: ['angular'], exports: 'angularResource'},
		angularRoute: {deps: ['angular'], exports: 'angularRoute'},
		bootstrap: {deps: ['angular'], exports: 'bootstrap'},
		angularFileUpload: {deps: ['angular', 'angularFileUploadShim'], exports: 'angularFileUpload'},
		xeditable:{deps: ['angular'], exports: 'xeditable'},
		uiBootstrapTpls: {deps: ['angular']},
		underscore: {exports: '_'},
	},
	waitSeconds: 20
});

//Start the main app logic.
require([  'angular',
           'checklistModel',
           'xeditable', 
           'angularResource',
           'angularRoute', 
           'uiBootstrapTpls',
           'angularFileUpload',
           'ngInfiniteScroll',
           'underscore',
           'controllers/controller'
           ],
           function(angular, xeditable, checklistModel, _) {
				'use strict';
				var app = angular.module('contentProviderApp', ['ngRoute', 
	                                                'ngResource',
	                                                'xeditable',
	                                                'ui.bootstrap',
	                                                'checklist-model',
	                                                'angularFileUpload',
	                                                'infinite-scroll',
	                                                'controllers',
	                                                'services',
	                                                'filters']);

				app.config(['$routeProvider', function($routeProvider, $routeParams) {
					$routeProvider
					.when('/', {
						templateUrl: 'resources/app/partials/default.html',
						controller: 'ContentProviderController'
					})
					.when('/networks', {
						templateUrl: 'resources/app/partials/networks.html',
						controller: 'ContentProviderController'
					})
					.otherwise({
						redirectTo: '/'
					});
				}]);
				app.run();
				angular.bootstrap(document, ['contentProviderApp']);
});


