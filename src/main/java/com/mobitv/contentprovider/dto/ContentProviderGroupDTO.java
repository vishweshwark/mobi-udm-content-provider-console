/**
 * 
 */
package com.mobitv.contentprovider.dto;

import java.util.Comparator;

/**
 * @author sbommareddy
 *
 */
public class ContentProviderGroupDTO implements Comparator<ContentProviderGroupDTO> {
	
	public String providerId;
	
	public String providerName;
	
	public String getProviderId() {
		return providerId;
	}
	
	public void setProviderId(String providerId) {
		this.providerId = providerId;
	}

	public String getProviderName() {
		return providerName;
	}

	public void setProviderName(String providerName) {
		this.providerName = providerName;
	}

	@Override
	public String toString() {
		return "ContentProviderGroupDTO [providerId=" + providerId
				+ ", providerName=" + providerName + "]";
	}

	@Override
	public int compare(ContentProviderGroupDTO o1, ContentProviderGroupDTO o2) {
		return (o1.providerName).compareTo(o2.providerName);
	}

}
