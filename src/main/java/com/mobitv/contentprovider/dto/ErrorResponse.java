package com.mobitv.contentprovider.dto;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import org.apache.log4j.Logger;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "error_response")
public class ErrorResponse {
		
	private static final Logger logger = Logger.getLogger(ErrorResponse.class);
	
	@XmlElement(name = "error_code")
	private String error_code = null;
	@XmlElement(name = "error_detail")
	private String error_detail = null;
	@XmlElement(name = "error_message")
	private String error_message = null;
	
	public ErrorResponse() {}

	public String getError_code() {
		return error_code;
	}

	public void setError_code(String error_code) {
		this.error_code = error_code;
	}

	public String getError_detail() {
		return error_detail;
	}

	public void setError_detail(String error_detail) {
		this.error_detail = error_detail;
	}

	public String getError_message() {
		return error_message;
	}

	public void setError_message(String error_message) {
		this.error_message = error_message;
	}

	@Override
	public String toString() {
		return "ErrorResponse [error_code=" + error_code + ", error_detail="
				+ error_detail + ", error_message=" + error_message + "]";
	}
	
}