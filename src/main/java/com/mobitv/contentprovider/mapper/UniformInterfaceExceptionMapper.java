package com.mobitv.contentprovider.mapper;

import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

import org.springframework.stereotype.Component;

import com.mobitv.platform.admin.dto.ErrorWrapper;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.UniformInterfaceException;

@Component
@Provider
public class UniformInterfaceExceptionMapper implements ExceptionMapper<UniformInterfaceException> {
	private int status;

	@Override
	public Response toResponse(UniformInterfaceException e) {
			status = e.getResponse().getStatus();
			ClientResponse response = e.getResponse();
			ErrorWrapper errorWrapper = e.getResponse().getEntity(ErrorWrapper.class);
			return Response.status(status).entity(errorWrapper).build();
		}


}
