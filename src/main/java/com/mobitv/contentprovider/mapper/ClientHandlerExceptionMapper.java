package com.mobitv.contentprovider.mapper;

import java.net.HttpURLConnection;

import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

import org.springframework.stereotype.Component;

import com.mobitv.exceptionwrapper.Errors;
import com.sun.jersey.api.client.ClientHandlerException;

@Component
@Provider
public class ClientHandlerExceptionMapper implements ExceptionMapper<ClientHandlerException> {

	@Override
	public Response toResponse(ClientHandlerException e) {
		return Response.status(HttpURLConnection.HTTP_INTERNAL_ERROR).entity(Errors.ExceptionMsg.setMessage(e.getClass() + ": " + (e.getMessage() == null ? "" : e.getMessage()))).build();
	}

}
