package com.mobitv.contentprovider.service;


import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.GregorianCalendar;
import java.util.Iterator;
import java.util.List;
import java.util.TimeZone;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileItemFactory;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.ldap.userdetails.LdapUserDetailsImpl;
import org.springframework.stereotype.Service;

import com.mobitv.commons.client.WebClient;
import com.mobitv.contentprovider.util.Utils;
import com.mobitv.platform.admin.dto.Asset;
import com.mobitv.platform.admin.dto.AssetList;
import com.mobitv.platform.admin.dto.Network;
import com.mobitv.platform.admin.dto.NetworkList;
import com.mobitv.platform.admin.dto.Thumbnail;
import com.mobitv.platform.admin.rest.ContentProviderService;
import com.sun.jersey.core.header.FormDataContentDisposition;
import com.sun.jersey.spi.resource.Singleton;

/**
 * @author kliolios
 * @date Mar 30, 2015
 *
 */
@Singleton
@Service
public class ContentProviderServiceImpl implements ContentProviderService  {
	/**
	 * 
	 */
	@Autowired
	@Qualifier("damWebClient")
	WebClient damWebClient;

	@Value("${network.special.characters.regex}")
	private String networkSpecialCharFilter;
	
	@Context 
	HttpServletRequest request;
    
	public NetworkList getNetworkList(String network_code) {
		return damWebClient.getResource()
				.path("entities/network")
				.queryParam("network_cd", (network_code == null ? "" : network_code))
				.accept(MediaType.APPLICATION_JSON)
				.get(NetworkList.class);
	}	

	public void deleteNetwork(String network_code) {
		damWebClient.getResource()
		.path("entities/network")
		.queryParam("network_cd", network_code)
		.accept(MediaType.APPLICATION_JSON)
		.delete();
	}

	public Network updateNetwork(Network network) {
		//		set the last_update_by value to the logged in user
		LdapUserDetailsImpl user = (LdapUserDetailsImpl)SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		String name = user.getUsername();		
		network.setLastUpdatedBy(name);
		boolean specialChar = false;
		String encodedValue = null;
		try {
			if(StringUtils.containsAny(network.getNetworkCd(),networkSpecialCharFilter)) {
				specialChar = true;
				encodedValue = URLEncoder.encode(network.getNetworkCd(), "UTF-8");
			}
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	
		if(specialChar) {
			return damWebClient.getResource()
				.path("entities/network")
				.queryParam("network_cd", encodedValue)
				.type(MediaType.APPLICATION_JSON)
				.accept(MediaType.APPLICATION_JSON)
				.put(Network.class, network);
		} else {
			return damWebClient.getResource()
				.path("entities/network")
				.queryParam("network_cd", network.getNetworkCd())
				.type(MediaType.APPLICATION_JSON)
				.accept(MediaType.APPLICATION_JSON)
				.put(Network.class, network);
		}
	}

	public AssetList getAssetList(
			String include_artifacts,
			String latest,
			String asset_id,
			String type,
			String provider_id,
			String alternate_asset_id,
			String external_asset_id,
			String network,
			String category,
			String name,
			String requires_location_check,
			String expired,
			Long start_time,
			Long end_time,
			String offset,
			String length,
			String sort_by,
			String sort_direction,
			String series_id,
			String audio_lang) {

		return damWebClient.getResource()
				.path("entities/asset").queryParam("include_artifacts", (include_artifacts == null ? "false" : include_artifacts))
				.queryParam("latest", (latest == null ? "false" : latest))
				.queryParam("asset_id", (asset_id == null ? "" : asset_id))
				.queryParam("type", (type == null ? "" : type))
				.queryParam("provider_id", (provider_id == null ? "" : provider_id))
				.queryParam("alternate_asset_id", (alternate_asset_id == null ? "" : alternate_asset_id))
				.queryParam("extenal_asset_id", (external_asset_id == null ? "" : external_asset_id))
				.queryParam("network", (network == null ? "" : network))
				.queryParam("category", (category == null ? "" : category))
				.queryParam("name", (name == null ? "" : name))
				.queryParam("requires_location_check", (requires_location_check == null ? "" : requires_location_check))
				.queryParam("expired", (expired == null ? "" : expired))
				//				.queryParam("start_time", (String) (start_time == null ? "" : start_time))
				//				.queryParam("end_time", (String) (end_time == null ? "" : end_time))
				.queryParam("offset", (offset == "0" ? "" : offset))
				.queryParam("length", (length == "100" ? "" : length))
				.queryParam("sort_by", (sort_by == "last_update_time" ? "" : sort_by))
				.queryParam("sort_direction", (sort_direction == "desc" ? "" : sort_direction))
				.queryParam("series_id", (series_id == null ? "" : series_id))
				.queryParam("audio_lang", (audio_lang == null ? "" :audio_lang))
				.accept(MediaType.APPLICATION_JSON)
				.get(AssetList.class);
	}

	public Asset updateAsset(Asset asset) {
		//		set the last_update_by value to the logged in user
		LdapUserDetailsImpl user = (LdapUserDetailsImpl)SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		String name = user.getUsername();		
		asset.setLastUpdateBy(name);

		//		set the last_udpate_time to the current date
		TimeZone utc = TimeZone.getTimeZone("UTC");
		GregorianCalendar gregorianCalendar = new GregorianCalendar(utc);
		DatatypeFactory datatypeFactory;
		try {
			datatypeFactory = DatatypeFactory.newInstance();
			XMLGregorianCalendar now = datatypeFactory.newXMLGregorianCalendar(gregorianCalendar);
			asset.setLastUpdateTime(now);
		} catch (DatatypeConfigurationException e) {
			throw new RuntimeException(e);
		}

		//		set the checksum to null
		asset.setChecksum(null);

		return damWebClient.getResource()
				.path("entities/asset")
				.type(MediaType.APPLICATION_JSON)
				.accept(MediaType.APPLICATION_JSON)
				.put(Asset.class, asset);

	}

	public Response getThumbnail(String thumbnail_id) {

		byte[] response = damWebClient.getResource()
				.path("entities/thumbnail/image")
				.queryParam("thumbnail_id", thumbnail_id)
				.accept("image/*")
				.get(byte[].class);

		return Response.ok(new ByteArrayInputStream(response)).build();
	}

	public Thumbnail postThumbnail(String assetId, String networkCd, String thumbnailName, String aspectRatio) {
		//		TODO - Validation of required fields: thumbnail_name, asset_id, format, create_by
		Thumbnail thumbnail = new Thumbnail();

		// kliolios: Since we use WADL we cannot take advantage
		// of jersey-multipart and because we are in
		// tomcat 6/servlet-api < 2.5, we have to use
		// apache commons fileupload to process multipart
		if (ServletFileUpload.isMultipartContent(request)) {
			final FileItemFactory factory = new DiskFileItemFactory();
			final ServletFileUpload fileUpload = new ServletFileUpload(factory);
			List items;
			try {
				items = fileUpload.parseRequest(request);

				if (items != null) {
					final Iterator iter = items.iterator();
					while (iter.hasNext())
					{
						final FileItem item = (FileItem) iter.next();
						if (item.isFormField()) {
							continue;
						} else {
							// Process form file field (input type="file").
							String fieldname = item.getFieldName();
							String filename = FilenameUtils.getName(item.getName());
							String format = item.getContentType();
							format = format.split("/")[1].toUpperCase();
							thumbnail.setFormat(format);
							InputStream filecontent = item.getInputStream();
							String imageData = Utils.encodeToString(filecontent);
							thumbnail.setImageData(imageData);
							thumbnail.setSize(item.getSize());

							thumbnail.setThumbnailAspectRatio(aspectRatio);
							//		always set file_type to "IMAGE" and the thumbnail_scheme to "base"
							thumbnail.setThumbnailName(thumbnailName);
							thumbnail.setFileType("IMAGE");
							thumbnail.setThumbnailScheme("base");
							String loggedInUser = ((LdapUserDetailsImpl)SecurityContextHolder.getContext().getAuthentication().getPrincipal()).getUsername();
							thumbnail.setCreateBy(loggedInUser);
						}
					}
				}			
			} catch (FileUploadException e) {
				throw new RuntimeException(e);
			} catch (IOException e) {
				throw new RuntimeException(e);
			}
		}

		try {
			if(StringUtils.containsAny(networkCd,networkSpecialCharFilter)) {
				networkCd = URLEncoder.encode(networkCd, "UTF-8");
			}
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		Thumbnail thumbnailResponse = damWebClient.getResource()
				.path("entities/thumbnail")
				.queryParam("asset_id", (assetId == null ? "" : assetId))
				.queryParam("network_cd", (networkCd == null ? "" : networkCd))
				.type(MediaType.APPLICATION_JSON)
				.post(Thumbnail.class, thumbnail);

		return thumbnailResponse;
	}

	/* (non-Javadoc)
	 * @see com.mobitv.platform.admin.rest.ContentProviderService#deleteThumbnailForAsset(java.lang.String)
	 */
	@Override
	public void deleteThumbnailForAsset(String thumbnail_id) {
		damWebClient.getResource()
		.path("entities/thumbnail")
		.queryParam("thumbnail_id", thumbnail_id)
		.accept(MediaType.APPLICATION_JSON)
		.delete();
	}

}

