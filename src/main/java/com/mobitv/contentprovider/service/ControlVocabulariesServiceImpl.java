/**
 * 
 */
package com.mobitv.contentprovider.service;

import static com.mobitv.contentprovider.util.ControlledVocabularyType.ASSET_STATE;
import static com.mobitv.contentprovider.util.ControlledVocabularyType.CATEGORIES;
import static com.mobitv.contentprovider.util.ControlledVocabularyType.EDITABLE_EXTENDED_METADATA;
import static com.mobitv.contentprovider.util.ControlledVocabularyType.GENRES;
import static com.mobitv.contentprovider.util.ControlledVocabularyType.THUMBNAIL_TYPE;

import java.io.IOException;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.mobitv.commons.client.WebClient;
import com.mobitv.platform.admin.dto.Ontology;
import com.mobitv.platform.admin.dto.TermList;
import com.mobitv.platform.admin.dto.Vocabulary;
import com.mobitv.platform.admin.rest.ControlVocabulariesService;
import com.sun.jersey.spi.resource.Singleton;

/**
 * @author kliolios
 * @date Feb 25, 2015
 *
 */
@Singleton
@Service
public class ControlVocabulariesServiceImpl implements ControlVocabulariesService {

	@Autowired
	@Qualifier("configManagerClient")
	WebClient configManagerClient;
	/* (non-Javadoc)
	 * @see com.mobitv.platform.admin.rest.ControlVocabulariesService#getCategories()
	 */
	@Override
	public TermList getCategoryTerms() {		
		return configManagerClient.getResource()
				.path("term_list")
				.queryParam("term_type", CATEGORIES.getName())
				.accept(MediaType.APPLICATION_JSON)
				.get(TermList.class);
	}

	/* (non-Javadoc)
	 * @see com.mobitv.platform.admin.rest.ControlVocabulariesService#getGenres(java.lang.String)
	 */
	@SuppressWarnings("unchecked")
	@Override
	public Ontology getGenreTerms() {
		String json = configManagerClient.getResource()
		.path("ontology")
		.queryParam("ontology_type", GENRES.getName())
		.accept(MediaType.APPLICATION_JSON)
		.get(String.class);

		ObjectMapper mapper = new ObjectMapper(); // can reuse, share globally

		Ontology ontology = new Ontology();
		
		try {
			ontology = mapper.readValue(json, Ontology.class);
		} catch (JsonParseException e) {
			throw new RuntimeException(e);
		} catch (JsonMappingException e) {
			throw new RuntimeException(e);
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
		return ontology;
	}

	/* (non-Javadoc)
	 * @see com.mobitv.platform.admin.rest.ControlVocabulariesService#getEditableExtendedMetadata()
	 */
	@Override
	public TermList getEditableExtendedMetadata() {
		return configManagerClient.getResource()
				.path("term_list")
				.queryParam("term_type", EDITABLE_EXTENDED_METADATA.getName())
				.accept(MediaType.APPLICATION_JSON)
				.get(TermList.class);
	}

	/* (non-Javadoc)
	 * @see com.mobitv.platform.admin.rest.ControlVocabulariesService#getAssetStatusTerms()
	 */
	@Override
	public Vocabulary getAssetStates() {
		return configManagerClient.getResource()
				.path("controlled_vocabulary")
				.queryParam("cv_type", ASSET_STATE.getName())
				.accept(MediaType.APPLICATION_JSON)
				.get(Vocabulary.class);
	}

	/* (non-Javadoc)
	 * @see com.mobitv.platform.admin.rest.ControlVocabulariesService#getThumbnailTypes()
	 */
	@Override
	public Vocabulary getThumbnailTypes() {	
		return configManagerClient.getResource()
				.path("controlled_vocabulary")
				.queryParam("cv_type", THUMBNAIL_TYPE.getName())
				.accept(MediaType.APPLICATION_JSON)
				.get(Vocabulary.class);
	}

}
