package com.mobitv.contentprovider.controller;

import javax.ws.rs.core.MediaType;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.mobitv.commons.client.WebClient;

@Controller
@RequestMapping("monitoring")
public class MonitorHealthController {
	@Autowired
	@Qualifier("damWebClient")
	WebClient damWebClient;
	
	@RequestMapping(value="health", method = RequestMethod.GET)
	@ResponseBody
	protected String monitoringHealthText() {
		return damWebClient.getResource()
			.path("/monitoring/health")
			.type(MediaType.TEXT_PLAIN)
			.get(String.class);
		
	}
}
