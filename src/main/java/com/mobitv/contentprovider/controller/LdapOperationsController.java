/**
 * 
 */
package com.mobitv.contentprovider.controller;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Properties;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.beans.factory.config.PropertiesFactoryBean;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.ldap.core.LdapTemplate;
import org.springframework.ldap.filter.AndFilter;
import org.springframework.ldap.filter.EqualsFilter;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.ldap.userdetails.LdapUserDetailsImpl;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.mobitv.commons.property.OverriddenPropertyLocations;
import com.mobitv.contentprovider.dto.ContentProviderGroupDTO;
import com.mobitv.contentprovider.util.ContentProviderAttributeMapper;

/**
 * @author sbommareddy
 *
 */
@Controller
@RequestMapping("admin/v5/content/ldap")
public class LdapOperationsController {
	private static final Logger logger = Logger.getLogger(LdapOperationsController.class);
	
	@Value("${ldap.contentprovider.base.dn}")
	private String baseDn;
	
	@Value("${ldap.contentprovider.group.name}")
	private String parentGroup;
	
	@Autowired
	@Qualifier("ldapTemplate")
	LdapTemplate ldapTemplate;
	
	@SuppressWarnings("unchecked")
	@RequestMapping(value="/contentprovidergroups/authorized", method = RequestMethod.GET)
	@ResponseBody
	protected List<ContentProviderGroupDTO> getAuthorizedContentProviderGroups() {
		List<GrantedAuthority> authorities = (List<GrantedAuthority>) SecurityContextHolder.getContext().getAuthentication().getAuthorities();
		
		List<ContentProviderGroupDTO> contentProviderGroupDtoList = new ArrayList<ContentProviderGroupDTO>();
		
		// TODO: Dinos:  Properties not being injected with spring 4.  Will have to fix.
		ApplicationContext ctx = new ClassPathXmlApplicationContext("/applicationContext.xml");
		Properties  propertyList = (Properties ) ctx.getBean("config-properties");
		baseDn = propertyList.getProperty("ldap.contentprovider.base.dn");
		parentGroup = propertyList.getProperty("ldap.contentprovider.group.name");
		
		for(int i=0; i<authorities.size(); i++ ) {
			logger.debug("authority " + i + ") " + authorities.get(i));
			
			String authority = authorities.get(i).getAuthority().toString().substring(5);
			AndFilter andFilter = new AndFilter();
			andFilter.and(new EqualsFilter("cn", authority));
			List<ContentProviderGroupDTO> attributeList = ldapTemplate.search(baseDn, andFilter.encode(), new ContentProviderAttributeMapper());
			
			if(attributeList.size() > 0) {
				logger.debug("providerId: "  +((ContentProviderGroupDTO) attributeList.get(0)).getProviderId());
				logger.debug("providerName: "  +((ContentProviderGroupDTO) attributeList.get(0)).getProviderName());
				if(!((ContentProviderGroupDTO) attributeList.get(0)).getProviderName().equalsIgnoreCase(parentGroup)) {
					contentProviderGroupDtoList.add((ContentProviderGroupDTO) attributeList.get(0));
				}
			}
		}
		
		Collections.sort(contentProviderGroupDtoList, new ContentProviderGroupDTO()); //ordering the list by provider_name
		return contentProviderGroupDtoList;
	}
	
	@SuppressWarnings("unchecked")
	@RequestMapping(value="/username", method = RequestMethod.GET)
	@ResponseBody
	protected String getLoggedInUser() {
		LdapUserDetailsImpl user = (LdapUserDetailsImpl)SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		return user.getUsername();			
		}	
	
	@SuppressWarnings("unchecked")
	@RequestMapping(value="/user", method = RequestMethod.GET)
	@ResponseBody
	protected LdapUserDetailsImpl getLoggedInUserDetails() {
		LdapUserDetailsImpl user = (LdapUserDetailsImpl)SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		return user;			
		}
		

}
