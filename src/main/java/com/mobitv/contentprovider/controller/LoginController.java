/**
 * 
 */
package com.mobitv.contentprovider.controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.ldap.userdetails.LdapUserDetailsImpl;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import org.springframework.web.servlet.ModelAndView;

/**
 * @author sbommareddy
 *
 */
@Controller
public class LoginController {
	
	@RequestMapping(value = "/", method = RequestMethod.GET)
	public ModelAndView welcomePage() {
 
		ModelAndView model = new ModelAndView();
		model.setViewName("index");
		
		LdapUserDetailsImpl user = (LdapUserDetailsImpl)SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		String name = user.getUsername();
		
		model.addObject("authenticatedUser", name);
		return model;
 
	}	
	
	@RequestMapping(value = "/test", method = RequestMethod.GET)
	public ModelAndView testPage() {
 
		ModelAndView model = new ModelAndView();
		model.setViewName("test");
		
		LdapUserDetailsImpl user = (LdapUserDetailsImpl)SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		String name = user.getUsername();
		ServletRequestAttributes attr = (ServletRequestAttributes)  RequestContextHolder.currentRequestAttributes();
		  HttpSession session= attr.getRequest().getSession(true);
		model.addObject("authenticatedUser", name);
		return model;
 
	}
	
	@RequestMapping(value = "/fileUpload", method = RequestMethod.GET)
	public ModelAndView fileUpload() {
		
		ModelAndView model = new ModelAndView();
		model.setViewName("fileUpload");
		
		return model;
	}
	
	@RequestMapping(value = "/login", method = RequestMethod.GET)
	public ModelAndView login(@RequestParam(value = "error", required = false) String error,
			@RequestParam(value = "logout", required = false) String logout) {

		ModelAndView model = new ModelAndView();
		if (error != null) {
			model.addObject("loginError", "Invalid username and/or password!");
		}

		if (logout != null) {
			model.addObject("msg", "You've been logged out successfully.");
		}
		model.setViewName("login");

		return model;

	}
	
	@RequestMapping(value = "/login/user", method = RequestMethod.GET)
	@ResponseBody
	protected String getUser() {
		LdapUserDetailsImpl user = (LdapUserDetailsImpl)SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		
		String name = user.getUsername(); //get logged in username
		
		return name;
	}
	
	@RequestMapping(value="/login/authorities", method = RequestMethod.GET)
	protected List<String> getAuthorities(ModelMap model) {
		@SuppressWarnings("unchecked")
		List<GrantedAuthority> authorities = (List<GrantedAuthority>) SecurityContextHolder.getContext().getAuthentication().getAuthorities();
		
		List<String> authoritiesList = new ArrayList<String>();
		
		for(int i=0; i<authorities.size(); i++ ) {
			authoritiesList.add(authorities.get(i).toString());
		}

		return authoritiesList;
	}
	
	@RequestMapping(value = "/logout", method = RequestMethod.GET)
	public ModelAndView logout(@RequestParam(value = "error", required = false) String error) {

		ModelAndView model = new ModelAndView();
		
		model.setViewName("logout");

		return model;

	}

}
