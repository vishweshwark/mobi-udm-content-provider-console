/**
 * 
 */
package com.mobitv.contentprovider.util;

import javax.naming.NamingException;
import javax.naming.directory.Attribute;
import javax.naming.directory.Attributes;

import org.springframework.ldap.core.AttributesMapper;

import com.mobitv.contentprovider.dto.ContentProviderGroupDTO;

/**
 * @author sbommareddy
 *
 */
public class ContentProviderAttributeMapper implements AttributesMapper {
	public Object mapFromAttributes(Attributes attributes) throws NamingException {
		ContentProviderGroupDTO contentProviderDTO = new ContentProviderGroupDTO();
		
//		TODO: For now using the "cn" and "description" fields in LDAP for the providerName and providerId respectively. Change these later if we are able to add custom attributes to LDAP schema
		Attribute description = attributes.get("description");
		Attribute cn = attributes.get("cn");
		if(description != null)
			contentProviderDTO.setProviderId((String)description.get());
		if(cn != null)
			contentProviderDTO.setProviderName((String)cn.get());
 
		return contentProviderDTO;
	}
}
