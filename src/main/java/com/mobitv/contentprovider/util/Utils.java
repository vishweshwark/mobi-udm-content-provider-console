/**
 * 
 */
package com.mobitv.contentprovider.util;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;

import org.apache.commons.codec.binary.Base64;
import org.springframework.web.multipart.MultipartFile;

/**
 * @author sbommareddy
 *
 */
public class Utils {
	
	public static String encodeToString(InputStream in ) {
		try {
			ByteArrayOutputStream bos = new ByteArrayOutputStream();
			byte[] buf = new byte[1024];
			int len;

			while ((len = in.read(buf)) > 0) {
				bos.write(buf, 0, len);
			}

			bos.flush();
			buf = bos.toByteArray();
			
            String imageString = Base64.encodeBase64String(buf);

            bos.close();
            return imageString;
            
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

}
