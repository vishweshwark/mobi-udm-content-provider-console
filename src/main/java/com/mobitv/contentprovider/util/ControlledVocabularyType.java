/**
 * 
 */
package com.mobitv.contentprovider.util;

/**
 * @author kliolios
 * @date Feb 26, 2015
 *
 */
public enum ControlledVocabularyType {
	CATEGORIES("categories", "Category"),
	GENRES("genres", "Genre"),
	ASSET_STATE("asset_state", "Asset State"),
	EDITABLE_EXTENDED_METADATA("editable_extended_metadata", "Editable Extended Metadata"),
	THUMBNAIL_TYPE("thumbnail_type", "Thumnbnail_type");

	private String name;
	private String displayName;
	
	private ControlledVocabularyType(String name, String displayName){
		this.name= name;
		this.displayName = displayName;
	}

	public String getName(){
		return name;
	}
	
	public String getDisplayName(){
		return displayName;
	}
}
